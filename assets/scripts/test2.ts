
import { _decorator, Component, Node, systemEvent, SystemEventType, Vec3, resources, Texture2D, MeshRenderer, Material } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('Test2')
export class Test2 extends Component {
    // [1]
    // dummy = '';

    // [2]
    // @property
    // serializableDummy = 0;

    start() {
        // [3]
    }
    onLoad() {
        // systemEvent.on(SystemEventType.TOUCH_START, this.beforeChange, this);
    }

    i: number = 0;

    // test2:Vec3[] = [new Vec3(1,1,1),new Vec3(2,2,2),new Vec3(3,3,3)];

    beforeChange() {

        //let self = this;
        this.i %= 5;
        this.i++;
        resources.load(this.i + "/texture", Texture2D, (err: any, texture: Texture2D) => {
            let meshRenderer: MeshRenderer = this.node.getComponent(MeshRenderer)!;

            // let a = meshRenderer.getMaterial(0)!;

            // let a = meshRenderer.getMaterial(0) as unknown as Material;

            let a: Material | null = meshRenderer.material!;

            a.setProperty("albedoMap", texture);
            // let b :Vec3 =  this.node.scale;
            console.log(a.getProperty("albedoMap"));
            // this.node.setScale(this.test2[this.i]);

        });

        // let meshRenderer : MeshRenderer= self.node.getComponent(MeshRenderer)!;

        // let a = meshRenderer.getMaterial(0)!;

        // // let a = meshRenderer.getMaterial(0) as unknown as Material;

        // //let a:Material| null = meshRenderer.material!;

        // a.setProperty("albedoMap",this.selfTexture);
        // console.log(a.getProperty("albedoMap"));


    }

    // update (deltaTime: number) {
    //     // [4]
    // }
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.0/manual/en/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.0/manual/en/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.0/manual/en/scripting/life-cycle-callbacks.html
 */
