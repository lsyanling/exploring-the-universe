
import { _decorator, Component, Node, Label, Layout, ToggleContainer, instantiate, Toggle, EventTouch, Button, Sprite, EventHandler } from 'cc';
import DivBlock from './DivBlock';
const { ccclass, property } = _decorator;

@ccclass('ListBox')
export class ListBox extends Component {
    @property({
        type: Label,
        tooltip:'目前所选内容文本'
    })
    optionLabel: Label = null!

    @property({
        type: Sprite,
        tooltip:'展开/收起listView时，切换的箭头sprite'
    })
    toggleButton: Sprite = null!

    @property({
        type: ToggleContainer,
        tooltip:'目前所选listView，需要其子节点有一个item'
    })
    listView: ToggleContainer = null!

    @property({
        type: [EventHandler],
        tooltip:'触发事件',
    })
    toggleEvent: EventHandler[] = [];

    listItem: Node = null!
    options: string[] = []
    nowOption: number = 0

    // [1]
    // dummy = '';

    // [2]
    // @property
    // serializableDummy = 0;

    start () {
        // this.setOptions([
        //     '红烧大爬虾',
        //     '清蒸太阳系',
        //     'bug大杂烩',
        //     '没有了'
        // ])
        // [3]
    }

    setOptions (option: string[], initOption: number = 0) {
        this.listItem = this.listView.node.children[0]
        this.updateListActive()
        this.options = option

        this.optionLabel.string = ''
        DivBlock.renderList(this.listView.node, option)
        if (option.length > 0) {
            this.listView.toggleItems[initOption]!.isChecked = true
            let toggle = this.listView.activeToggles()
            this.optionLabel.string = toggle[0]!.node.getComponentInChildren(Label)!.string
        }
    }

    setChoice (toggle: Toggle, data: string) {
        if (this.options.length == 0) return
        this.optionLabel.string = toggle.node.getComponentInChildren(Label)!.string
        this.nowOption = this.listView.toggleItems.indexOf(toggle)
        this.toggleEvent.forEach((event) => {
            event.emit([this.nowOption, event.customEventData])
        })
        this.toggleList()
        // this.toggleEvent.emit([this.nowOption, this.toggleEvent.customEventData])
        
    }

    toggleList (toggleEvent: EventTouch = null!, data: string = null!) {
        this.updateListActive(!this.listView.node.active)
        
    }

    updateListActive (active = false) {
        this.listView.node.active = active
    }
    // update (deltaTime: number) {
    //     // [4]
    // }
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.0/manual/en/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.0/manual/en/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.0/manual/en/scripting/life-cycle-callbacks.html
 */
