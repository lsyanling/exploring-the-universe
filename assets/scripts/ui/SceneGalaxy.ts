// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { _decorator, Node, Button, Label, Prefab, AudioClip, SpriteAtlas, Event, EventTouch, instantiate, view, game, color, Sprite, Toggle, ToggleContainer, LightComponent, Layers, EditBox, tween, Tween } from 'cc';
const { ccclass, property } = _decorator;

import DivBlock from '../ui/DivBlock';
import AudioCtrl from '../core/AudioCtrl';
import Setting from "../ui/Setting";
import GalaxyManager from '../game/GalaxyManager';
import FacilityData from '../data/FacilityData';
import UserManager from '../core/UserManager';
import GalaxyData from '../data/GalaxyData';
import { UpgradeBoard } from './UpgradeBoard';
import PlanetData from '../data/PlanetData';
import { EmergencyBoard } from './EmergencyBoard';
import { EDITOR } from 'cc/env';

@ccclass('SceneGalaxy')
export default class SceneGalaxy extends DivBlock {
    @property({
        type: Node,
        tooltip: '返回按钮'
    })
    backButton: Node = null!;

    @property({
        type: Prefab,
        tooltip: '设置界面'
    })
    settingPrefab: Prefab = null!

    @property({
        type: AudioClip,
        tooltip: '关卡音乐'
    })
    bgm: AudioClip = null!

    @property({
        type: GalaxyManager,
        tooltip: '行星管理器'
    })
    manager: GalaxyManager = null!

    @property({
        type: [ToggleContainer],
        tooltip: '星系各星球名称listView'
    })
    listViews: ToggleContainer[] = []

    @property({
        type: Node,
        tooltip: '行星信息面板'
    })
    planetPanel: Node = null!;

    @property({
        type: [Node],
        tooltip: '弹出面板集合'
    })
    boards: Node[] = [];

    @property({
        type: UpgradeBoard,
        tooltip: '升级组件'
    })
    upgrade: UpgradeBoard = null!

    @property({
        type: EmergencyBoard,
        tooltip: '升级组件'
    })
    emergency: EmergencyBoard = null!

    @property({
        type: [DivBlock],
        tooltip: '使用的一些scene'
    })
    scenes: DivBlock[] = []

    chosenPlanet: number = -1

    settingNode: any = null
    // LIFE-CYCLE CALLBACKS:
    onLoad() {
        super.onLoad()
        this.manager.sceneCtrl = this
        this.upgrade.node.active = false
        this.emergency.node.active = false
        // 读取存档
        this.settingNode = instantiate(this.settingPrefab)
        this.settingNode.parent = this.node;
        this.settingNode.active = false;
    }

    start() {
        // super.start()
        try {
            AudioCtrl.playBgm(this.bgm, 1)
        } catch (error) {
            console.log('放bgm出错', this.bgm);
            console.error(error);
        }
        // UserManager.clearSaveData()  // 每次进游戏清空存档，便于测试
        let solar = UserManager.getGalaxy(0)
        if (!solar) {
            // 未创建太阳系
            UserManager.registerNewGalaxy(0)
        }

        this.manager.galaxies = UserManager.getGalaxy()
        this.manager.node.active = true
        this.gameStart()
    }


    refresh () {
        const block1 = this.blocks[0].getComponent(DivBlock)!
        block1.refresh()
    }

    blockEnter(id: number) {
        super.blockEnter(id)
        if (this.stack.length >= 2 && this.backButton) {
            let params = { time: this.transtionTime };
            this.nodeEnter(this.backButton, params)
        }
    }

    blockBack() {
        super.blockBack()
        if (this.stack.length <= 1 && this.backButton) {
            let params = { time: this.transtionTime };
            this.nodeExit(this.backButton, params)
        }
    }

    blockBackByButton() {
        if (this.canOperate) {
            this.blockBack()
            this.manager.setGalaxy()
        }
    }

    callFacility(event: EventTouch, data: string) {
        let btn = event.currentTarget as Node
        // id是选中的设施在设施列表里的位置
        let id = btn.parent!.parent!.getSiblingIndex()
        let facility: FacilityData | null = this.currentPlanet()!.buildings[id]
        AudioCtrl.playSet('decision') // 播放确认se
        this.upgrade.show(this.currentPlanet()!, facility, data != 'degrade')
    }

    callUpgrade(event: EventTouch, data: string) {
        if (data === 'close') {
            AudioCtrl.playSet('cancel') // 播放确认se
        } else {
            AudioCtrl.playSet('decision') // 播放确认se
        }
    }

    callSetting(event: EventTouch, data: string) {
        AudioCtrl.playSet('decision') // 播放确认se
        this.settingNode.active = true
        let settingCom: Setting = this.settingNode.getComponent(Setting)
        settingCom.onSetting()
    }


    callSwitch() {
        let callback = this.gameStart
        this.blockHide()

        this.stack.pop()

        let params = { time: this.transtionTime };
        this.nodeExit(this.backButton, params)

        this.manager.hideGalaxy(callback, this)
    }

    /**
     * 暂停按钮
     */
    callPause(event: EventTouch, data: string) {
        UserManager.userData['pause'] = !UserManager.userData['pause']
        GalaxyManager.instance.requestRunning(true)
        this.refresh()
    }

    callNameEdit(editBox: EditBox, data: string) {
        if (editBox.string) {
            this.currentPlanet()!.name = editBox.string
            UserManager.saveData()
            // 重新渲染
            this.refresh()
        } else {
            editBox.string = this.currentPlanet()!.name
        }
    }

    endGame(event: EventTouch, data: string) {
        game.end();
    }


    // 从左边listView选择星球回调
    choosePlanet(toggle: Toggle, data: string) {
        if (EDITOR) {
            console.log('gun');
            
            return
        }
        let index = toggle.node.getSiblingIndex()
        this.chosenPlanet = toggle.isChecked ? index : -1
        const block1 = this.blocks[0].getComponent(DivBlock)!
        if (this.chosenPlanet >= 0) {
            this.manager.requestRunning(false)
            block1.blockChange(1)
            console.log(block1.node.active);
            
            this.renderPlanetPanel()
            console.log('labeltest',toggle.getComponentInChildren(Label));
        } else {
            let container = toggle.node.parent!.getComponent(ToggleContainer)!
            if (!container.anyTogglesChecked()) {
                block1.blockChange(0)
                this.manager.focus()
            }
        }
    }

    // todo: 鼠标点击界面空白处：点中星球就选中，选中星球时没点中星球，取消星球选中
    callBlank(event: EventTouch, data: string) {
        // todo: 通过射线检测判断是否点中星球，拿到点中的星球，以及星球index等
        // 选中与否注意：通过listView的回调也能改是否选中，两种方式要融合好
        if (this.chosenPlanet) {
            this.cancelPlanet()

        } else {

        }
    }

    // 取消星球选中
    cancelPlanet() {
        const block1 = this.blocks[0].getComponent(DivBlock)!
        block1.blockChange(0)
        this.manager.focus()
        this.manager.requestRunning(true)

    }

    gameStart(event: EventTouch = null!, data: string = null!) {
        if (!this.canOperate) return
        let seed = UserManager.userData['nowGalaxy']
        this.manager.waiting = true
        this.manager.preLoad(seed)
    }

    callPlanet() {
        if (this.stack.indexOf(0) === -1) {
            this.blockEnter(0)
        } else {
            this.blockShow()
        }
    }


    renderPlanetPanel() {
        let index = this.chosenPlanet
        if (index < 0) return
        const planetData = this.currentPlanet()!
        this.manager.focus(index)
        let list: any = {
            name: planetData.name, // 这些数值从index对应的Planetdata取的，如这个是planetData.name
            kind: index === 0 ? '恒星' : '行星',
            planetRadius: planetData.radius + ' km',       // list属性名是要渲染节点的名称，注意和实际取的属性名不一定对应
            orbitRadius: planetData.orbitalRadius.toFixed(3) + ' AU',
            resource: planetData.abundance,
            layout: [] // 取星球的建筑，如果数量没超上限，再加[null]
        }
        // 星球数据表格
        list.resource = list.resource.concat(
            planetData.hard.map((hard) => {
                return ['低', '中', '高'][hard]
            })
        )
        .concat(PlanetData.getProfit(planetData).map(value => {
            let abs = Math.abs(value)
            if (value > 0) {
                return '+' + abs.toFixed(1)
            } else if (value < 0) {
                return '-' + abs.toFixed(1)
            }
            return '0'
        }))
        .concat(PlanetData.getCapacity(planetData))
        // 建筑卡片
        planetData.buildings.forEach((building) => {
            list.layout.push([this.getNowGalaxy(), planetData, building])
        })
        if (list.layout.length < PlanetData.maxFacilityNum(planetData)) list.layout.push([null])
        // 渲染
        const renderNode = this.planetPanel.getChildByName('Layout')!
        DivBlock.render(renderNode, list, true)
    }

    setEmergency(title: string, description: string) {
        this.manager.requestRunning(false)
        this.emergency.show(title, description)
    }

    currentPlanet() {
        if (this.chosenPlanet >= 0) {
            return this.getNowGalaxy().planets[this.chosenPlanet]
        } else {
            return null
        }
    }

    getNowGalaxy(): GalaxyData {
        let seed = UserManager.userData['nowGalaxy']
        return UserManager.getGalaxy(seed)
    }
}

