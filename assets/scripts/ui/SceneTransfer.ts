
import { _decorator, Component, Node, ToggleContainer, EditBox, Prefab, Button, EventTouch, RichText } from 'cc';
import DataManager from '../core/DataManager';
import UserManager from '../core/UserManager';
import GalaxyData from '../data/GalaxyData';
import DivBlock from './DivBlock';
import { GalaxyItem } from '../ui_items/GalaxyItem';
import SceneGalaxy from './SceneGalaxy';
import { TinyItem } from '../ui_items/TinyItem';
const { ccclass, property } = _decorator;

@ccclass('SceneTransfer')
export class SceneTransfer extends DivBlock {
    @property({
        type: [ToggleContainer],
        tooltip:'星系各星球名称listView'
    })
    listViews: ToggleContainer[] = []

    @property({
        type: [Prefab],
        tooltip:'使用的prefab'
    })
    prefabs: Prefab[] = []

    sceneCtrl: SceneGalaxy = null!
    posBox: EditBox[] = []
    resourceBox: EditBox[] = []
    seedList: number[] = []
    transMax: number[] = []
    explore: number[] = []
    nowOption: number = -1
    reversed: boolean = false
    // [1]
    // dummy = '';

    // [2]
    // @property
    // serializableDummy = 0;

    setBlock (fromBlock: DivBlock | null = null) {
        if (fromBlock) {
            // 带ctrl意味着是从别的画面进来的
            this.sceneCtrl = fromBlock as SceneGalaxy
            this.sceneCtrl.manager.requestRunning(false)
            this.sceneCtrl.manager.hideGalaxy()
            this.nowOption = -1
            DivBlock.selectListView(this.listViews[0])
            DivBlock.selectListView(this.listViews[1], 0)
            this.blockChange(0)
        }
        const left = this.node.children[0]
        const right = this.node.children[1]
        const galaxy = this.sceneCtrl.getNowGalaxy()
        // 1. 左侧面板
        const nowPanel = left.getChildByName('galaxy_panel')!.getComponent(GalaxyItem)!
        nowPanel.setValue(galaxy)
        // 下方其它星系
        const listView = this.listViews[0]
        let list: any = []
        this.seedList = []
        for (const seed in UserManager.userData['galaxies']) {
            const numSeed = Number(seed)
            if (numSeed != UserManager.userData['nowGalaxy']) {
                list.push([UserManager.getGalaxy(numSeed)])
                this.seedList.push(numSeed)
            }
        }
        DivBlock.childrenStackSort(listView.node, list.length, listView.node.children[0])
        DivBlock.render(listView.node, list)
        // 2. 右侧面板
        const selection = this.listViews[1]
        list = ['数据概览', '星系探索']
        let keys = UserManager.getGalaxy()
        if (Object.keys(keys).length > 1) {
            list.push('资源转运')
        }

        DivBlock.childrenStackSort(selection.node, list.length, selection.node.children[0])
        DivBlock.render(selection.node, list)
        this.renderDataBoard()
        this.renderExplore()
        this.renderTransfer()
        

        // [3]
    }

    selectCurrent (event: EventTouch ,data: string) {
        const toggle = this.listViews[0].activeToggles()[0]
        if (toggle) {
            toggle.isChecked = false
        }
        this.nowOption = -1
        this.reversed = false
        this.renderDataBoard()
        this.renderTransfer()
    }
    
    selectOther (event: EventTouch ,data: string) {
        const toggle = this.listViews[0].activeToggles()[0]
        if (toggle) {
            this.nowOption = this.listViews[0].toggleItems.indexOf(toggle)
        } else {
            this.nowOption = -1
        }
        this.reversed = false
        this.renderDataBoard()
        this.renderTransfer()
    }
    
    renderDataBoard () {
        const board = this.blocks[0].getChildByName('Layout')!
        const galaxy = this.currentGalaxy()
        const button = this.blocks[0].getChildByName('Button')!.getComponent(Button)!
        let list = {
            name: galaxy.name,
            starnum: '星球数量: ' + galaxy.planets.length,
            settled: '定居时间: ' + UserManager.getTimeString(galaxy.settleTime),
            pos: UserManager.getPosString(this.nowOption == -1 ? UserManager.userData['nowGalaxy'] : this.seedList[this.nowOption])
        }
        list = DivBlock.render(board, list)
        // console.log('dataBoard', list);
        
    }
    
    renderExplore () {
        const board = this.blocks[1].getChildByName('Layout')!
        const button = this.blocks[1].getChildByName('Button')!.getComponent(Button)!
        const galaxy = this.sceneCtrl.getNowGalaxy()
        let list: any = {
            from_title: `当前 <color=#80C2FF>${galaxy.name} <color=#ffffff><color=#FFFFFF>星系资源存量：`,
            from_storage: [],
            condition: [],
            position: []
        }
        // 当前星系存量
        let fromStorage = galaxy.resourceSurplus
        list.from_storage = fromStorage.map((value, i) => {
            return [`icon_res_${i}`, Math.floor(value)]
        })
        // 坐标处理
        let axis = ['X', 'Y', 'Z']
        axis.forEach((value) => {
            list.position.push({
                axis: value,
                editbox: 0
            })
        })
        // 探索消费
        let cost = UserManager.getExploreCost()
        cost.forEach((value, i) => {
            if (value != 0) {
                let resName = DataManager.dataAssets['dataSystem']['resName'][i]
                // if (facility instanceof Object && facility.broken) cost /= 2
                let ok = this.sceneCtrl.getNowGalaxy().resourceSurplus[i] >= value
                let item = [`<img src="icon_res_${i}"/>` + resName, value, ok]
                list.condition.push(item)
            }
        })
        const frs = board.getChildByName('.from_storage')!
        const condition = board.getChildByName('.condition')!
        const position = board.getChildByName('.position')!
        DivBlock.childrenStackSort(frs, list.from_storage.length, frs.children[0])
        DivBlock.childrenStackSort(condition, list.condition.length, condition.children[0])
        DivBlock.childrenStackSort(position, list.position.length, position.children[0])
        list = DivBlock.render(board, list)
        list.position.forEach((value: any, i: number) => {
            this.posBox[i] = value['editbox']
        });
        this.checkExplore()
    }
    
    renderTransfer () {
        const board = this.blocks[2].getChildByName('Layout')!
        const galaxy = this.nowOption != -1 ? this.reversed ? this.currentGalaxy() : this.sceneCtrl.getNowGalaxy() : this.sceneCtrl.getNowGalaxy()
        const button = this.blocks[2].getChildByName('Button')!.getComponent(Button)!
        button.interactable = false
        let keys = UserManager.getGalaxy()
        if (Object.keys(keys).length === 1) return
        let list: any = {
            cost: '',
            from_title: `当前 <color=#80C2FF>${galaxy.name}<color=#ffffff><color=#FFFFFF> 资源存量：`,
            to_title: '',
            action: '请先选择目标星系',
            from_storage: [],
            to_storage: [],
            editnum: [],
            trans: this.nowOption != -1 ? 'trans_' + this.reversed : ''
        }
        let fromStorage = galaxy.resourceSurplus
        list.from_storage = fromStorage.map((value: number, i: any) => {
            return [`icon_res_${i}`, Math.floor(value)]
        })
        const editnum = board.getChildByName('.editnum')!
        const ct = board.getChildByName('.cost_title')!
        const tos = board.getChildByName('.to_storage')!
        ct.active = editnum.active = tos.active = this.nowOption != -1
        if (this.nowOption != -1) {
            const targetGalaxy = this.reversed ? this.sceneCtrl.getNowGalaxy() : this.currentGalaxy()
            list.action = `<color=#80C2FF>${galaxy.name}<color=#ffffff> 星系\n转运至\n<color=#80C2FF>${targetGalaxy.name}<color=#ffffff> 星系`
            list.cost = 0
            // 
            list.to_title = `<color=#80C2FF>${targetGalaxy.name}<color=#ffffff><color=#FFFFFF> 星系仓库空余：`
            let storage = targetGalaxy.resourceSurplus
            let capacity = GalaxyData.getMaxCapacity(targetGalaxy)
            list.to_storage = storage.map((value: number, i: number) => {
                let restCapacity = Math.max(0, capacity[i] - value)
                this.transMax[i] = Math.floor(restCapacity)
                return [`icon_res_${i}`, Math.ceil(restCapacity)]
            })
            for (let i = 0; i < 3; i++) {
                list.editnum.push([`icon_res_${i}`, 0])
            }
        }
        list = DivBlock.render(board, list, true)
        list.editnum.forEach((value: TinyItem, i: number) => {
            let node = value.node.getChildByName('.value')!
            this.resourceBox[i] = node.getComponent(EditBox)!
        });
        const r = list.action as RichText
        console.log(r.node.children);
    }

    getTransferCost(res: number[] = []) {
        const rate = UserManager.techChecked(0, 7) ? 0.05 : 0.1
        let num = 0
        res.forEach(element => {
            num += Math.ceil(rate * element)
        });
        return num
    }

    checkTransfer(box: EditBox, data: string) {
        const rate = UserManager.techChecked(0, 7) ? 0.05 : 0.1 // 转运消耗能源比例，可以科技树降低        
        const board = this.blocks[2].getChildByName('Layout')!
        const button = this.blocks[2].getChildByName('Button')!.getComponent(Button)!
        const nowResource = this.reversed ? this.currentGalaxy().resourceSurplus : this.sceneCtrl.getNowGalaxy().resourceSurplus
        const targetGalaxy = this.reversed ? this.sceneCtrl.getNowGalaxy() : this.currentGalaxy()
        const nowValue = Number(box.string)
        let energyCost = 0
        let index = this.resourceBox.indexOf(box)
        for (let i = 0; i < 3; i++) {
            if (i != index) energyCost += Math.ceil(rate * Number(this.resourceBox[i].string))
        }
        let max = 0
        // 算最大转量
        if (index == 0) {
            max = Math.floor((nowResource[0] - energyCost) / (1 + rate))
        } else {
            max = Math.floor(Math.min(nowResource[index], (nowResource[0] - energyCost) / rate))
        }
        // console.log('maxTrans', max, );
        
        // 算最大存量
        let storage = targetGalaxy.resourceSurplus
        let capacity = GalaxyData.getMaxCapacity(targetGalaxy)
        let restCapacity = Math.max(0, capacity[index] - storage[index])
        // 更新实际最大值和界面
        max = Math.min(Math.ceil(restCapacity), max)
        box.string = Math.max(0, Math.min(nowValue, max)).toFixed(0)
        let cost = this.getTransferCost(this.resourceBox.map((box) => {
            return Number(box.string)
        }))
        let list = {cost: cost}
        list = DivBlock.render(board, list)
        button.interactable = cost > 0
    }

    callTransfer (event: EventTouch ,data: string) {
        let res = this.resourceBox.map((box) => {
            return Number(box.string)
        })
        if (this.reversed) {
            UserManager.resTransfer(this.currentGalaxy(), this.sceneCtrl.getNowGalaxy(), res)
        } else {
            UserManager.resTransfer(this.sceneCtrl.getNowGalaxy(), this.currentGalaxy(), res)
        }
        this.setBlock()
    }

    getExploreSeed () {
        return UserManager.getSeedNumber(this.posBox[0].string, this.posBox[1].string, this.posBox[2].string)
    }

    checkExplore (box: EditBox | null = null, data: string = null!) {
        if (box) {
            if (box.string) {
                let num = Number(box.string)
                if (num < 0) {
                    box.string = String(-num)
                }
                
            } else {
                box.string = '0'
            }
        }
        const galaxy = this.sceneCtrl.getNowGalaxy()
        const button = this.blocks[1].getChildByName('Button')!.getComponent(Button)!
        let resEnough = GalaxyData.getResEnough(galaxy, UserManager.getExploreCost())
        let seedOk = UserManager.checkSeedOk(this.getExploreSeed())
        button.interactable = resEnough && seedOk
        return button.interactable
    }

    callSwitch (event: EventTouch ,data: string) {
        const seed: number = this.nowOption == -1 ? UserManager.userData['nowGalaxy'] : this.seedList[this.nowOption]
        // console.log('switch', seed, this.nowOption);
        UserManager.userData['nowGalaxy'] = seed
        UserManager.saveData()
        this.sceneCtrl.callSwitch()
    }

    callReverse(event: EventTouch ,data: string) {
        this.reversed = !this.reversed
        this.renderTransfer()
    }

    callExplore (event: EventTouch ,data: string) {
        const galaxy = this.sceneCtrl.getNowGalaxy()
        let resEnough = GalaxyData.getResEnough(galaxy, UserManager.getExploreCost())
        if (this.checkExplore() && resEnough) {
            const seed = this.getExploreSeed()
            GalaxyData.gainResource(galaxy, UserManager.getExploreCost(), true)
            UserManager.registerNewGalaxy(seed)
            this.sceneCtrl.callSwitch()
        }

    }

    callNameEdit (editBox: EditBox, data: string) {
        editBox.string = UserManager.setGalaxyName(this.currentGalaxy(), editBox.string)
        this.setBlock()
    }

    currentGalaxy () {
        if (this.nowOption != -1) {
            return UserManager.getGalaxy(this.seedList[this.nowOption])
        }
        return this.sceneCtrl.getNowGalaxy()
    }
    // update (deltaTime: number) {
    //     // [4]
    // }
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.0/manual/en/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.0/manual/en/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.0/manual/en/scripting/life-cycle-callbacks.html
 */
