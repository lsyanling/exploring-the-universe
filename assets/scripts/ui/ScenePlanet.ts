
import { _decorator, Component, Node } from 'cc';
import UserManager from '../core/UserManager';
import GalaxyData from '../data/GalaxyData';
import PlanetData from '../data/PlanetData';
import GalaxyManager from '../game/GalaxyManager';
import DivBlock from './DivBlock';
import SceneGalaxy from './SceneGalaxy';
const { ccclass, property } = _decorator;

@ccclass('ScenePlanet')
export class ScenePlanet extends DivBlock {
    ctrl: SceneGalaxy = null!
    // [1]
    // dummy = '';

    // [2]
    // @property
    // serializableDummy = 0;

    setBlock (from: DivBlock | null) {
        if (from) {
            this.ctrl = from as SceneGalaxy
        }
        GalaxyManager.instance.requestRunning(true)
        super.setBlock(from)
    }

    refresh() {
        const galaxy = this.ctrl.getNowGalaxy()
        let list: any = {
            Operation: {
                name: galaxy.name, // 这些数应该赋值成userdata中特定星球的属性
                state: GalaxyData.getStateString(galaxy),
                prosperity: '',//'繁荣度：' + 452,
                time: UserManager.getTimeString(),
                pause: UserManager.userData['pause'] ? 'btn_continue' : 'btn_pause'
            },
            ListView: galaxy.planets.map((planet, i) => {
                return planet.name
            }),
            Resource: []
        }
        // 资源容量计算
        for (let i = 0; i < 3; i++) {
            list.Resource.push([galaxy, i])
        }
        list = DivBlock.render(this.node, list, true)
        this.ctrl.renderPlanetPanel()
    }

    renderPlanetPanel() {
        let index = this.ctrl.chosenPlanet
        if (index < 0) return
        const planetData = this.ctrl.currentPlanet()!
        this.ctrl.manager.focus(index)
        let list: any = {
            name: planetData.name, // 这些数值从index对应的Planetdata取的，如这个是planetData.name
            kind: index === 0 ? '恒星' : '行星',
            planetRadius: planetData.radius + ' km',       // list属性名是要渲染节点的名称，注意和实际取的属性名不一定对应
            orbitRadius: planetData.orbitalRadius.toFixed(3) + ' AU',
            resource: planetData.abundance,
            layout: [] // 取星球的建筑，如果数量没超上限，再加[null]
        }
        list.resource = list.resource.concat(
            planetData.hard.map((hard) => {
                switch (hard) {
                    case 0:
                        return '低'
                    case 1:
                        return '中'
                    case 2:
                        return '高'
                    default:
                        return '—'
                }
            })
        )
        .concat(PlanetData.getProfit(planetData).map(value => {
            let abs = Math.abs(value)
            if (value > 0) {
                return '+' + abs.toFixed(1)
            } else if (value < 0) {
                return '-' + abs.toFixed(1)
            }
            return '0'
        }))
        .concat(PlanetData.getCapacity(planetData))

        planetData.buildings.forEach((building) => {
            list.layout.push([this.ctrl.getNowGalaxy(), planetData, building])
        })
        if (list.layout.length < PlanetData.maxFacilityNum(planetData)) list.layout.push([null])
        // 渲染
        const renderNode = this.ctrl.planetPanel.getChildByName('Layout')!
        const facilityLayout = renderNode.getChildByName('.layout')!
        DivBlock.childrenStackSort(facilityLayout, list.layout.length, facilityLayout.children[0])
        DivBlock.render(renderNode, list)
    }

    // update (deltaTime: number) {
    //     // [4]
    // }
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.0/manual/en/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.0/manual/en/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.0/manual/en/scripting/life-cycle-callbacks.html
 */
