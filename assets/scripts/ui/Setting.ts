// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { _decorator, Component, Toggle, Slider, director, Layout, Widget } from 'cc';
import AudioCtrl from '../core/AudioCtrl';
import UserManager from '../core/UserManager';
const {ccclass, property} = _decorator;


@ccclass('Setting')
export default class Setting extends Component {
    @property({
        tooltip: '静音和跳过剧情的按钮',
        type: [Toggle]
    })
    toggles: Toggle[] = []
    @property({
        tooltip: '音量音效滑动条',
        type: [Slider]
    })
    sliders: Slider[] = []
    skipNode = null
    userSettings: any = null
    // LIFE-CYCLE CALLBACKS:
    onLoad () {
    }
    onSetting () {
        
        // 立刻执行布局和对齐，因为要暂停了，不给你机会
        this.node.children[1]!.getComponent(Layout)!.updateLayout()
        this.node.children.forEach(node => {
            let widget: Widget = node.getComponent(Widget)!
            if (widget) widget.updateAlignment()
        });
        this.userSettings = UserManager.getUserSettings()
        this.toggles[0].isChecked = this.userSettings.mute
        this.toggles[1].isChecked = this.userSettings.skip
        this.sliders[0].progress = this.userSettings.musicVolume
        this.sliders[1].progress = this.userSettings.soundVolume
        director.pause()
    }

    setMute () {
        UserManager.getUserSettings().mute = this.toggles[0].isChecked
        AudioCtrl.adjustMusicVolume()
    }
    
    setSkip () {
        UserManager.getUserSettings().skip = this.toggles[1].isChecked
    }
    
    setMusicVolume () {
        UserManager.getUserSettings().musicVolume = this.sliders[0].progress
        AudioCtrl.adjustMusicVolume()
    }
    
    setSoundVolume () {
        UserManager.getUserSettings().soundVolume = this.sliders[1].progress
        //AudioCtrl.adjustSoundVolume()
    }
    
    back () {
        director.resume()
        UserManager.saveData()
        this.node.active = false


    }
    // update (dt) {}
}