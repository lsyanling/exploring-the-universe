
import { _decorator, Component, Node, Prefab, UITransform, instantiate, Vec3, Vec2, Label, Sprite, Toggle, ToggleContainer, EventTouch, Button, color, Graphics, SpriteFrame } from 'cc';
import AudioCtrl from '../core/AudioCtrl';
import DataManager from '../core/DataManager';
import UserManager, { TechData } from '../core/UserManager';
import DivBlock from './DivBlock';
import SceneGalaxy from './SceneGalaxy';
const { ccclass, property } = _decorator;


@ccclass('SceneTechTree')
export class SceneTechTree extends DivBlock {
    // [1]
    @property({
        type: [SpriteFrame],
        tooltip:'按钮图片'
    })
    buttonImages: SpriteFrame[] = []

    @property({
        type: Node,
        tooltip: 'itemContent'
    })
    itemContent: Node = null!;

    @property({
        type: Node,
        tooltip: 'itemContent'
    })
    layoutContent: Node = null!;

    @property({
        type: Button,
        tooltip: '研究按钮'
    })
    resButton: Button = null!;

    techRefer: number[][] = []
    sceneCtrl: SceneGalaxy = null!
    nowRefer: number[] = []
    // dummy = '';

    // [2]
    // @property
    // serializableDummy = 0;

    start () {
        super.start()

        // [3]
    }

    setBlock (from: DivBlock | null = null) {
        if (from) {
            this.sceneCtrl = from as SceneGalaxy
            this.sceneCtrl.manager.requestRunning(false)
            this.sceneCtrl.manager.hideGalaxy()
        }
        const height = this.itemContent.getComponent(UITransform)!.height
        const data = DataManager.dataAssets['dataTechTree']
        const toggleContainer = this.itemContent.children[0].getComponent(ToggleContainer)!
        const g = toggleContainer.getComponent(Graphics)!;
        g.clear()
        let maxDepth = 0
        this.techRefer = []
        data.forEach((branch: TechData[], i: number) => {
            let depth = 0
            for (let j = 0; j < branch.length; j++) {
                const item = branch[j];
                if (UserManager.canSetTech(i, j)) {
                    this.setSingle(item, i, j, this.techRefer.length)
                    depth = j % 5 + 1
                    this.techRefer.push([i, j])
                    if (depth > maxDepth) {
                        maxDepth = depth
                    }
                }
            }
        });
        this.itemContent.getComponent(UITransform)!.width = maxDepth * 200 - 40
        let list = [this.sceneCtrl.getNowGalaxy(), 2]
        DivBlock.render(this.node.children[2], list)
        if (from) {
            DivBlock.selectListView(toggleContainer, -1, true)
            this.setBoard()
        }
    }

    setSingle (item: TechData, i: number, j: number, counter: number) {
        const buttonContainer = this.itemContent.children[0]
        const labelContainer = this.itemContent.children[1]
        const g = buttonContainer.getComponent(Graphics)!;
        let x = j % 5 * 200
        let y = i * 216 + Math.floor(j / 5) * 73
        let buttonNode = buttonContainer.children[counter] ? buttonContainer.children[counter] : instantiate(buttonContainer.children[0])
        let labelNode = labelContainer.children[counter] ? labelContainer.children[counter] : instantiate(labelContainer.children[0])
        const sprite = buttonNode.getComponent(Sprite)!
        const label = labelNode.getComponent(Label)!
        label.string = item.name
        const pos = new Vec3(x + 80, 648 - y - 20, 0)
        const checked = UserManager.techChecked(i, j)
        g.strokeColor.fromHEX(checked ? '#66ff66' : '#669966')
        sprite.spriteFrame = checked ? this.buttonImages[1] : this.buttonImages[0]
        if (j >= 1 && j < 5) {
            g.moveTo(x, pos.y)
            g.lineTo(x - 40, pos.y)
        } else if (j >= 5) {
            g.moveTo(pos.x, 648 - y)
            g.lineTo(pos.x, 648 - y + 35)
        }
        g.stroke()
        // console.log(pos, i, j);
        buttonNode.setPosition(pos)
        labelNode.setPosition(pos)
        buttonContainer.addChild(buttonNode)
        labelContainer.addChild(labelNode)
    }
    
    setBoard (refer: number[] | null = null) {
        this.nowRefer = refer ? refer : []
        let list: any = {
            name: '科技研发',
            state: '',
            requirement: '请于左侧选择研发项目',
            description: '科学技术的进步是探索宇宙不可缺少的一环。做好科学技术资源储备可以为星际探索事业增动赋能。\n请于左侧研发计划中选择项目，即可预览研发项目的信息。\nAI助手将继续为您服务。',  // todo: 把这段正常的文案改中二点。。。
            condition: []
        }
        let ok = [false, false]
        let buttonActive = false
        let stateColor = '#ff9999'
        if (refer) {
            const data: TechData = DataManager.dataAssets['dataTechTree'][refer[0]][refer[1]]
            list.name = data.name
            list.description = data.description
            list.requirement = '研发条件'
            ok[0] = this.sceneCtrl.getNowGalaxy().resourceSurplus[2] >= data.cost
            let item = [`<img src="icon_res_2"/>` + DataManager.dataAssets['dataSystem']['resName'][2], data.cost, ok[0]]
            list.condition.push(item)
            if (refer[1] != 0) {
                // 前置科技
                let demand = refer[1] < 5 ? refer[1] - 1 : refer[1] % 5
                ok[1] = UserManager.canSetTech(refer[0], refer[1])
                const name = DataManager.dataAssets['dataTechTree'][refer[0]][demand].name
                list.condition.push([`前置科技 <color=#acdaff>${name}<color=#ffffff>`, '', ok[1]])
            } else {
                ok[1] = true
            }
            if (UserManager.techChecked(refer[0], refer[1])) {
                // 已研发
                list.state = '已研发'
                stateColor = '#99ff99'
            } else {
                list.state = '未研发'
                buttonActive = true
                this.resButton.interactable = ok[0] && ok[1]
            }
        }
        const condition = this.layoutContent.getChildByName('.condition')!
        DivBlock.childrenStackSort(condition, list.condition.length)
        list = DivBlock.render(this.layoutContent, list)
        const label = list.state as Label
        label.color = color(stateColor)
        this.resButton.node.active = buttonActive
    }
    
    checkSelect (toggle: Toggle, data: string) {
        if (!this.canOperate) return
        let index = toggle.node.getSiblingIndex()
        let refer = this.techRefer[index]
        if (toggle.isChecked) {
            this.setBoard(refer)
        } else {
            let container = toggle.node.parent!.getComponent(ToggleContainer)!
            if (!container.anyTogglesChecked()) {
                this.setBoard()
            }
        }
    }

    callResearch (event: EventTouch, data: string) {
        if (this.nowRefer.length > 0) {
            const galaxy = this.sceneCtrl.getNowGalaxy()
            if (UserManager.setTech(galaxy, this.nowRefer[0], this.nowRefer[1])) {
                AudioCtrl.playSet('buy')
                this.setBlock()
                this.setBoard(this.nowRefer)
            } else {
                AudioCtrl.playSet('invalid')
            }
        }
    }
    
    // update (deltaTime: number) {
    //     // [4]
    // }
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.0/manual/en/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.0/manual/en/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.0/manual/en/scripting/life-cycle-callbacks.html
 */
