
import { _decorator, Component, Node, RichText, Layout, Prefab, Button } from 'cc';
import DataManager from '../core/DataManager';
import UserManager from '../core/UserManager';
import FacilityData from '../data/FacilityData';
import PlanetData from '../data/PlanetData';
import GalaxyManager from '../game/GalaxyManager';
import { DivBoard } from '../ui_base/DivBoard';
import DivBlock from './DivBlock';
import { ListBox } from './ListBox';
const { ccclass, property } = _decorator;

@ccclass('UpgradeBoard')
export class UpgradeBoard extends DivBoard {
    @property({
        type: RichText,
        tooltip: '行为描述富文本'
    })
    optionText: RichText = null!

    @property({
        tooltip: '展示变化和条件的两个layout。',
        type: [Layout]
    })
    layouts: Layout[] = []

    @property({
        type: ListBox,
        tooltip: '选建需要的listBox'
    })
    listBox: ListBox = null!

    @property({
        type: [Prefab],
        tooltip: '需要的比较项和条件项预制'
    })
    itemPrefabs: Prefab[] = []

    upgrade: boolean = false
    planet: PlanetData = null!
    facility: FacilityData | null = null
    okList: any[] = []
    newFacilityId = 0
    newFacilityLevel = 0

    close() {
        super.close()
        GalaxyManager.instance.sceneCtrl.renderPlanetPanel()
    }

    show(planet: PlanetData, facility: FacilityData | null, upgrade = true) {
        super.show()
        this.planet = planet
        this.facility = facility
        this.upgrade = upgrade
        let list: any = {
            title: '设施',
            action_title: '请选择需要修建的设施',
            demand_title: '所需条件：',
            btn_action: '升级'
        }
        if (upgrade) {
            this.listBox.node.active = !facility
            if (facility) {
                if (facility.broken) {
                    list.btn_action = '修复'
                } else {
                    let name = FacilityData.getFacilityName(facility)
                    let level = 'I'.repeat(facility.level + 1)
                    list.action_title = `升级建筑 <color=#acdaff>${name}<color=#ffffff> 至等级 <color=#0cdaff>${level}<color=#ffffff> 效果：`
                }
                this.setDemand(planet, facility)
            } else {
                list.btn_action = '建造'
                this.okList = PlanetData.availableFacilities(planet, GalaxyManager.instance.sceneCtrl.getNowGalaxy())
                if (this.okList.length == 0) {
                    list.demand_title = ''
                    list['description'] = '目前暂无设施可以修建'
                    list.title = list.btn_action + list.title
                    this.listBox.setOptions([])
                    const button = this.node.children[0].getChildByName('.btn_action')!.getComponent(Button)!
                    button.interactable = false
                    DivBlock.render(this.node.children[0], list, true)
                    return
                } else {
                    list.demand_title = '所需条件：'
                    let nameList = this.okList.map((type) => {
                        return FacilityData.getFacilityName(type, FacilityData.getMinLevel(this.planet, type))
                    })
                    this.listBox.setOptions(nameList)
                    let nowChoice = this.okList[0]
                    this.setDemand(planet, nowChoice, FacilityData.getMinLevel(this.planet, nowChoice))
                }
            }
            list.demand_title = list.btn_action + list.demand_title
        } else if (facility) {
            this.listBox.node.active = false
            if (facility.broken) {
                list.btn_action = '修复'
                list.demand_title = list.btn_action + list.demand_title
            } else {
                list.btn_action = facility.level == FacilityData.getMinLevel(planet, facility.type) ? '拆除' : '降级'
                list.demand_title = list.btn_action + '设施返还资源：'
            }
            this.setDemand(planet, facility)
            let name = FacilityData.getFacilityName(facility)
            list.action_title = `将要${list.btn_action}的 <color=#acdaff>${name}<color=#ffffff> 设施效果：`
        } else {
            console.warn('错误：降级空建筑');
            return
        }
        list.title = list.btn_action + list.title
        DivBlock.render(this.node.children[0], list, true)
    }

    /**
     * 
     * @param planet 
     * @param facility 有建筑为建筑，无建筑为类型
     * @param level 没有建筑，为建到的等级-1
     * @returns 
     */
    private setDemand(planet: PlanetData, facility: FacilityData | number, level: number = 0) {
        const upgrade = this.upgrade
        let list: any = {
            demand_layout: [],
            effect_layout: []
        }
        const type = facility instanceof Object ? facility.type : facility
        level = facility instanceof Object ? facility.level : level
        // 0.计算要显示的当前等级和左边等级
        let oldLv = -1
        let nowLv = 1
        if (facility instanceof Object) {
            let minLv = FacilityData.getMinLevel(planet, facility.type)
            if (!facility.broken) {
                nowLv = upgrade ? facility.level + 1 : level == minLv ? minLv : level - 1
                oldLv = !upgrade && level == minLv ? -1 : level
            } else {
                nowLv = level
            }
        } else {
            this.newFacilityId = facility
            this.newFacilityLevel = nowLv = level
        }
        const data = DataManager.dataAssets['dataFacility'][type]
        list['description'] = data.description
        // 1. 计算效果栏展示
        // 计算矿井产量
        if (type >= 1 && type <= 3 || type == 5) {
            const i = type == 5 ? 0 : type - 1
            let resName = DataManager.dataAssets['dataSystem']['resName'][i]
            const production = PlanetData.getResFactor(this.planet, i) * (nowLv + 1) * UserManager.getProductionBuff(i)
            let item = [resName + '产能', production.toFixed(1), '/月']
            if (oldLv >= 0) {
                const productionOld = PlanetData.getResFactor(this.planet, i) * (oldLv + 1) * UserManager.getProductionBuff(i)
                item.push(productionOld.toFixed(1)) // 旧的产能
            }
            list.effect_layout.push(item)
        }
        // 计算各资源容量
        let capacity = FacilityData.getCapacity(type, nowLv)
        let oldCapacity = oldLv >= 0 ? FacilityData.getCapacity(type, oldLv) : null
        capacity.forEach((value, i) => {
            let resName = DataManager.dataAssets['dataSystem']['resName'][i]
            if (value != 0) {
                let item = [resName + '容量', value, '']
                if (oldCapacity) {
                    item.push(oldCapacity[i])
                }
                list.effect_layout.push(item)
            }
        })
        // 能源消耗
        if (type >= 2 && type <= 4) {
            // 矿井：计算产出
            let item = ['能源消耗', nowLv * FacilityData.energyPerLevel(), '/月']
            if (oldLv >= 0) {
                item.push(oldLv * FacilityData.energyPerLevel()) // 当前等级产能
            }
            list.effect_layout.push(item)
        }
        // 2. 计算各资源要求
        let costRes = []
        if (upgrade) {
            costRes = FacilityData.getUpgradeCost(facility, level)
        } else {
            if (facility instanceof Object) {
                let minLv = FacilityData.getMinLevel(planet, facility.type)
                costRes = facility.broken ? FacilityData.getFixCost(facility) : facility.level == minLv ? FacilityData.getRemoveReturn(facility) : FacilityData.getDegradeReturn(facility)
            } else {
                console.warn('没建筑还降级');
                return
            }
        }
        costRes.forEach((cost: number, i: number) => {
            if (cost <= 0) return
            const resName = DataManager.dataAssets['dataSystem']['resName'][i]
            let ok = this.upgrade || facility instanceof Object && facility.broken ? GalaxyManager.instance.sceneCtrl.getNowGalaxy().resourceSurplus[i] >= cost : null
            let item = [`<img src="icon_res_${i}"/>` + resName, cost, ok]
            list.demand_layout.push(item)

        });
        DivBlock.render(this.node.children[0], list, true)
        const button = this.node.children[0].getChildByName('.btn_action')!.getComponent(Button)!
        button.interactable = this.canDecision()
    }

    toggleDemand(id: number, data: string) {
        let type = this.okList[id]
        let level = FacilityData.getMinLevel(this.planet, type)
        this.setDemand(this.planet, type, level)
    }

    // 是否可以升级
    canDecision() {
        const galaxy = GalaxyManager.instance.sceneCtrl.getNowGalaxy()
        const data = this.facility instanceof Object ? this.facility : this.newFacilityId
        return FacilityData.canOperate(galaxy, this.planet, data, this.upgrade)
    }

    callDecision() {
        const galaxy = GalaxyManager.instance.sceneCtrl.getNowGalaxy()
        const data = this.facility instanceof Object ? this.facility : this.newFacilityId
        FacilityData.operate(galaxy, this.planet, data, this.upgrade)
        GalaxyManager.instance.sceneCtrl.refresh()
        this.close()
    }

    // update (deltaTime: number) {
    //     // [4]
    // }
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.0/manual/en/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.0/manual/en/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.0/manual/en/scripting/life-cycle-callbacks.html
 */
