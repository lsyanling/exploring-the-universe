
import { _decorator, Component, Node, assert, tween, UIOpacity, UITransform, v3, view, Widget, EventTouch, Vec3, Label, ProgressBar, Sprite, Prefab, instantiate, RichText, EditBox, ToggleContainer, Tween, Button, Toggle, Layout } from 'cc';
import AudioCtrl from '../core/AudioCtrl';
import GameControl from '../core/GameControl';
import UiItem from '../ui_base/UiItem';
const { ccclass, property } = _decorator;

interface NodeTweenParam {
    callback?: Function     // 回调函数
    time: number            // 动作时间
    distance?: number       // 移动距离
    dir?: number            // 方向
    sound?: string
}

@ccclass('DivBlock')
export default class DivBlock extends Component {
    @property({
        tooltip: '收纳的各模块内容。',
        type: [Node]
    })
    blocks: Node[] = []

    @property({
        tooltip: '从模块递进时，保留显示的内容',
        type: [Node]
    })
    subItems: Node[] = []
    
    @property({
        tooltip: '初始渐入第一个模块',
    })
    firstFadeIn: boolean = true
    
    @property({
        tooltip: '渐变时间'
    })
    transtionTime: number = 0.5
    
    protected stack: number[] = []
    canOperate = true

    onLoad () {
        //console.log(`节点${this.node.name}读取`);
        
        this.blocks.forEach((node) => {
            node.active = false
            let opacity = node.getComponent(UIOpacity)
            if (!opacity) {
                node.addComponent(UIOpacity)
                //console.log(node.name+ '已添加UIOpacity组件');
            }
            node.children.forEach(child => {
                let opacity = child.getComponent(UIOpacity)
                child.active = false
                if (!opacity) {
                    child.addComponent(UIOpacity)
                    //console.log(child.name+ '已添加UIOpacity组件');
                }
            })
        })
    }

    start () {
        // [3]
        if (this.firstFadeIn && this.blocks[0]) {
            this.blockEnter(0)
            
        }
    }

    protected setBlock (fromBlock: DivBlock | null = null) {
        this.refresh()
    }

    refresh() {

    }
    /**
     * #zh 激活并播放一个节点的出现动画 
     * 
     * @protected
     * @param node 要做动作的节点
     * @param params 节点进入动作的相关参数
     */
    protected nodeEnter (node: Node, params: NodeTweenParam) {
        let widget = node.getComponent(Widget)
        if (widget) {
            widget.updateAlignment()
        }
        
        let transform = node.getComponent(UITransform)!
        let opacity = node.getComponent(UIOpacity)!
        assert(transform && opacity, node.name + "节点缺少UItrans组件或者UIopacity组件")
        let w = transform.width;
        let h = transform.height;
        let size = view.getCanvasSize()
        opacity.opacity = 0;
        if (params.sound) AudioCtrl.playSe(params.sound, 1)
        // 对参数的默认值设置
        params.callback = params.callback ? params.callback : (node: Node) => {};
        let pos = node.position.clone()
        let movePos = v3()
        let dir = Math.sign(pos.x);
        if (Math.abs(pos.x) >= 0.2 * size.width) {
            if (!params.distance) params.distance = w;
            movePos.x = -params.distance * dir
        } else {
            dir = pos.y >= 0 ? 1 : -1;
            if (!params.distance) params.distance = h;
            movePos.y = -params.distance * dir
        }
        node.setPosition(node.position.subtract(movePos))
        //console.log([node.name, node.position.x, node.position.y]);
        node.active = true;
        //console.log(`节点${node.name}进入[${pos.x}, ${pos.y}][${movePos.x}, ${movePos.y}]`);
        Tween.stopAllByTarget(opacity)
        tween(node)
        .to(params.time, { position: pos}, {easing: 'cubicOut'})
        .call(params.callback.bind(this))
        .start()
        tween(opacity)
        .to(params.time, {opacity: 255})
        .start()
    }
    
    /**
     * #zh 播放一个节点的消失动画并取消激活
     * 
     * @protected
     * @param node 要做动作的节点
     * @param params 节点进入动作的相关参数
     */
    protected nodeExit (node: Node, params: NodeTweenParam) {
        let transform = node.getComponent(UITransform)!
        let opacity = node.getComponent(UIOpacity)!
        assert(transform && opacity, node.name + "节点缺少UItrans组件或者UIopacity组件")
        let w = transform.width;
        let h = transform.height;
        let size = view.getCanvasSize()
        if (params.sound) AudioCtrl.playSe(params.sound, 1)
        // 对参数的默认值设置
        params.callback = params.callback ? params.callback : (node: Node) => {};
        let pos = node.position.clone()
        let movePos = v3()
        let dir = Math.sign(pos.x);
        if (Math.abs(pos.x) >= 0.2 * size.width) {
            if (!params.distance) params.distance = w;
            movePos.x = params.distance * dir
        } else {
            dir = pos.y >= 0 ? 1 : -1;
            if (!params.distance) params.distance = h;
            movePos.y = params.distance * dir
        }
        
        ////console.log([movePos, movePos.negative()]);
        
        tween(node)
        .by(params.time, { position: movePos}, {easing: 'cubicOut'})
        .by(0, { position: movePos.clone().negative()})
        .call(params.callback.bind(this))
        .start()

        tween(opacity)
        .to(params.time, {opacity: 0})
        .call((opacity: UIOpacity) => {
            opacity.node.active = false
        })
        .start()
        //console.log(`节点${node.name}移出[${node.position.x}, ${node.position.y}][${movePos.x}, ${movePos.y}]`);
    }
    
    /**
     * 使特定块渐入
     * @protected
     * @param id 块id
     * @param backward 是否是退出某块时操作，退出到该block
     */
    protected blockFadeIn (id: number, backward: boolean = false) {
        const blockNode = this.blocks[id]
        
        assert(blockNode, `要进入的block序号出界：${id}`)
        blockNode.active = true;
        let subItems: Node[] = []
        let targetBlock = blockNode.getComponent(DivBlock)
        if (targetBlock) {
            //subItems = targetBlock.blocks
            // 回退时：非保留显示内容才需要渐入
            if (backward) subItems = subItems.concat(targetBlock.subItems)
        }
        blockNode.children.forEach((node: Node) => {
            if (subItems.indexOf(node) === -1) {
                let params = {time: this.transtionTime};
                this.nodeEnter(node, params);
            }
        })
        if (targetBlock) {
            targetBlock.setBlock(this)
        }
        //console.log(`节点${this.node.name}渐入块${blockNode.name}`);
    }

    /**
     * 使特定块渐出
     * @protected
     * @param id 块id
     * @param backward 是否是退出某块时操作，使该block退出
     */
    protected blockFadeOut (id: number, backward: boolean = false) {
        const blockNode = this.blocks[id]
        assert(blockNode, `要进入的block序号出界：${id}`)
        let subItems: Node[] = []
        let targetBlock = blockNode.getComponent(DivBlock)
        if (targetBlock) {
            //subItems = targetBlock.blocks
            // 递进时：非保留显示内容才需要退出
            if (!backward) subItems = subItems.concat(targetBlock.subItems)
        }
        blockNode.children.forEach((node: Node) => {
            if (subItems.indexOf(node) === -1) {
                let params = {time: this.transtionTime};
                this.nodeExit(node, params);
            }
        })
        //console.log(`节点${this.node.name}渐出块${blockNode.name}`);
    }

    /**
     * 动画期间暂停操作
     */
    protected pauseOperation () {
        this.canOperate = false;
        tween(this.node)
        .delay(this.transtionTime)
        .call(() => {
            this.canOperate = true;
        })
        .start()
    }

    /**
     * 递进
     * @param id 递进到的id
     */
    blockEnter (id: number) {
        this.pauseOperation()
        assert(this.stack.indexOf(id) === -1, `进入的block序号 ${id} 已在栈中：${this.stack}`)
        
        if (this.stack.length > 0) {
            let outId = this.stack[this.stack.length - 1]
            this.blockFadeOut(outId)
        }
        this.blockFadeIn(id)
        this.stack.push(id)
    }

    /**
     * 回退
     */
    blockBack () {
        this.pauseOperation()
        assert(this.stack.length > 0, `不能在栈无元素回退：${this.stack}`)

        let outId = this.stack.pop()!
        this.blockFadeOut(outId, true)
        if (this.stack.length > 0) {
            let inId = this.stack[this.stack.length - 1]!
            this.blockFadeIn(inId, true)
        }
    }

    /**
     * 替换栈最后的块
     * @param id 替换为的id
     */
    blockChange (id: number) {
        this.pauseOperation()
        // 栈顶有块则出栈
        if (this.stack.length > 0) {
            let outId = this.stack.pop()!
            if (outId == id) {
                this.stack.push(id)
                return
            }
            this.blockFadeOut(outId, true)
        }

        assert(this.stack.indexOf(id) === -1, `进入的block序号 ${id} 已在栈中：${this.stack}`)
        // 入栈
        this.blockFadeIn(id)
        this.stack.push(id)
    }

    /**
     * 暂时隐藏块
     * @param id 替换为的id
     */
    blockShow () {
        this.pauseOperation()
        // 栈顶有块则出栈
        let id = this.stack[this.stack.length - 1]
        this.blockFadeIn(id)
    }

    /**
     * 暂时隐藏块
     * @param id 替换为的id
     */
    blockHide () {
        this.pauseOperation()
        // 栈顶有块则出栈
        if (this.stack.length > 0) {
            let outId = this.stack[this.stack.length - 1]
            this.blockFadeOut(outId, true)
        }
    }

    /**
     * 一次批量退出块[未实装]
     * @param time 退出次数
     */
    blockExit (time: number = 1) {
        //blockNode.children.forEach((node: Node) => {
        //let params = {time: this.transtionTime};
        //this.nodeExit(node, params);

        //});
    }

    
    blockEnterByButton (event: EventTouch, data: string) {
        let id = Number(data);
        if (this.canOperate) this.blockEnter(id)
    }

    blockBackByButton (event: EventTouch, data: string) {
        if (this.canOperate) this.blockBack()
    }

    blockChangeByButton (event: EventTouch, data: string) {
        let id = Number(data);
        if (this.canOperate) this.blockChange(id)
    }

    /**
     * [按钮调用]按照ListView里面点的按钮的位置替换块
     * @param event 传入输入事件
     * @param data 填入offset
     */
    blockChangeByList (event: EventTouch, data: string) {
        let offset = Number(data);
        if (!(offset >= 0)) offset = 0  // <0 和 NaN处理
        let node = event.getCurrentTarget() as Node
        let id = offset + node.getSiblingIndex()
        this.blockChange(id)
        let toggle: Button = node.getComponent(Button)!
        const con = node.parent!.getComponent(ToggleContainer)
        if (con) {
            Tween.stopAllByTarget(toggle)
            con.toggleItems.forEach((toggle) => {
                tween(toggle)
                .call((toggle: Toggle) => {
                    toggle.enabled = false
                })
                .delay(this.transtionTime)
                .call((toggle: Toggle) => {
                    toggle.enabled = true
                })
                .start()
            })

        }
    }

    /**
     * 整理某节点的激活子节点数为特定数量，默认以children[0]为模板
     * @param node 根节点
     * @param num 需要的子节点数量
     * @param model 模板
     * @returns 无
     */
    static childrenStackSort (node: Node, num: number = 0, model: Prefab | Node | null = null) {
        if (!model) model = node.children[0]
        let i = 0
        for (; i < num; i++) {
            let child = node.children[i];
            if (!child) {
                if (model) {
                    child = instantiate(model) as Node
                    node.addChild(child)
                } else {
                    console.warn('整理子节点未找到模板');
                    return
                }
            }
            child.name = '.' + i
            child.active = true
        }
        for (; i < node.children.length; i++) {
            let child = node.children[i];
            child.active = false
        }
        let layout = node.getComponent(Layout) 
        if (layout) {
            layout.updateLayout()
            //console.log('sort', num, layout);
        }
        // 刷新widget
        for (i = 0; i < num; i++) {
            let widget = node.children[i].getComponent(Widget)
            if (widget) {
                widget.updateAlignment()
            }
        }
    }

    /**
     * 通过读取子节点名称当作属性名，自动拿label等组件的渲染方法，比一个个拿组件在渲染方便多了
     * @param node 根基节点
     * @param object 递归遍历的属性对象
     * @param autoList 为true会自动以children[0]为模板调整长度渲染
     * @returns 传入的object的value都会换成指定渲染组件
     */
    static render(node: Node, object: any, autoList: boolean = false) {
        if (object instanceof Array) {
            // 传入了数组属性，一项项找名字为 .index 子节点渲染
            let uiItem = node.getComponent(UiItem)
            // console.log('.' + key);
            if (uiItem) {
                // 有UiItem组件调用其设置方法，把value数组传参
                uiItem.setValue.apply(uiItem, object) 
                return uiItem
            } else {
                // 没有，按照下标渲染，如果
                if (autoList) {
                    DivBlock.renderList(node, object)
                } else {
                    for (let i = 0; i < object.length; i++) {
                        const element = object[i];
                        let son = node.getChildByName('.' + i)
                        object[i] = this.renderSingle(son, element, autoList)
                        
                    }
                }
                /*
                let layout = node.getComponent(Layout) 
                if (layout) layout.updateLayout()
                object.forEach((com) => {
                    if (com instanceof Component) {
                        let widget = com.getComponent(Widget)
                        if (widget) widget.updateAlignment()
                        console.log('widget', widget, widget?.getComponent(UITransform));
                        
                    }
                })*/
                return object
            }
        } else {
            for (const key in object) {
                const value = object[key]
                let child = node.getChildByName('.' + key)
                if (value instanceof Object) {
                    if (child) {
                        object[key] = DivBlock.render(child, value, autoList)
                    }
                } else {
                    // 对象或字符串
                    object[key] = this.renderSingle(child, value, autoList)
                }
            }
            return object
        }
    }

    static renderList (node: Node, list: any[], prefab: Prefab | Node | null = null) {
        prefab = prefab ? prefab : node.children[0]
        DivBlock.childrenStackSort(node, list.length, prefab)
        return DivBlock.render(node, list)
    }

    private static renderSingle (node: Node | null, value: any, autoList = false) {
        if (node) {
            if (value instanceof Object) {
                return DivBlock.render(node, value, autoList)
            } else {
                value = value.toString()
                // 1.自带label/richtext
                let label = node.getComponent(Label) || node.getComponent(RichText) || node.getComponent(EditBox)
                if (label) {
                    label.string = label instanceof RichText ? '<color=#ffffff>' + value + '<color=#ffffff>': value // 艺术字加特殊前后缀，是解决变色bug的
                    if (label instanceof RichText) {
                        label.node.children.forEach(child => {child.active = true})
                    }
                    return label
                } 
                // 2.名为value子节点带label
                let valueChild = node.getChildByName('value')
                if (valueChild) {
                    // console.log(valueChild);
                    label = valueChild.getComponent(Label)
                    if (label) {
                        // console.log(label);
                        label.string = value
                        return label
                    }
                }
                // 3.progressbar
                let progressbar = node.getComponent(ProgressBar)
                let num = Number(value)
                if (progressbar && value != NaN) {
                    progressbar.progress = num
                    return progressbar
                }
                // 4.sprite换图集
                let sprite = node.getComponent(Sprite)
                if (sprite) {
                    if (value) {
                        sprite.node.active = true
                        let image = GameControl.instance.atlasAssets[0].getSpriteFrame(value)
                        if (image) {
                            sprite.spriteFrame = image
                        } else {
                            console.warn('iconset找不到spriteFrame: ', GameControl.instance.atlasAssets[0], value);
                        }
                    } else {
                        sprite.node.active = false
                    }
                    return sprite
                }

            }
        }
        console.warn('未能成功渲染', node, value);
        
        return value
    }

    /**
     * 快捷操作：选中ToggleContainer的一个toggle
     * @param container 
     * @param option 所选项，-1为取消当前选中
     * @param noCallback 不回调
     * @returns 选中的toggle
     */
    static selectListView (container: ToggleContainer, option = -1, noCallback = false) {
        if (option >= 0) {
            const toggle = container.toggleItems[option]
            if (toggle) {
                if (noCallback) {
                    toggle.setIsCheckedWithoutNotify(true)
                } else {
                    toggle.isChecked = true
                }
                return toggle
            } else {
                console.warn('设定的ListView没有特定选项', container, option);
            }
        } else {
            const toggle = container.activeToggles()[0]
            if (toggle) {
                if (noCallback) {
                    toggle.setIsCheckedWithoutNotify(false)
                } else {
                    toggle.isChecked = false
                }
            }
            return false
        }
    }
    // update (deltaTime: number) {
    //     // [4]
    // }
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.0/manual/en/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.0/manual/en/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.0/manual/en/scripting/life-cycle-callbacks.html
 */
