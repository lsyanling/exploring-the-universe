
import { _decorator, Component, Node, Layout } from 'cc';
import GalaxyManager from '../game/GalaxyManager';
import { DivBoard } from '../ui_base/DivBoard';
import DivBlock from './DivBlock';
const { ccclass, property } = _decorator;

@ccclass('EmergencyBoard')
export class EmergencyBoard extends DivBoard {
    @property({
        tooltip: '展示变化和条件的两个layout。',
        type: Node
    })
    layout: Node = null!

    // [1]
    // dummy = '';

    // [2]
    // @property
    // serializableDummy = 0;

    show (title: string, description: string) {
        super.show()
        let list = {
            title: title,
            description: description
        }
        DivBlock.render(this.layout, list)
    }

    close () {
        super.close()
        GalaxyManager.instance.requestRunning(true)
        GalaxyManager.instance.sceneCtrl.refresh()
    }

    // update (deltaTime: number) {
    //     // [4]
    // }
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.0/manual/en/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.0/manual/en/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.0/manual/en/scripting/life-cycle-callbacks.html
 */
