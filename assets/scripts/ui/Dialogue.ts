// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { _decorator, Component, Label, Node } from 'cc';
const {ccclass, property} = _decorator;


@ccclass('Dialogue')
export default class Dialogue extends Component {
    @property({
        tooltip: "对话内容和点击继续节点",
        type: [Label]
    })
    contents: Label[] = []
    // LIFE-CYCLE CALLBACKS:
    // onLoad () {}
    targetNode: Node | null = null
    start () {

    }
    setDialog (node: Node, string: string, params = {}) {
        //this.targetNode = node
        // 节点的缩放情况变化时，使得节点始终为正常缩放
        //this.update(0.013);
        // let num = Utils.randInt(3) + 1;
        // if (string.length >= 40) {
        //     AudioCtrl.playSe("crazydaveextralong" + num, 1);
        // } else if (string.length >= 20) {
        //     AudioCtrl.playSe("crazydavelong" + num, 1);
        // } else {
        //     AudioCtrl.playSe("crazydaveshort" + num, 1);
        // }
        //this.contents[0].string = string
        //this.update(0.013)

    }
    close () {
        //this.node.destroy();
    }
    update (dt: number) {
        //let pos = this.targetNode.convertToWorldSpaceAR(cc.v2(0, this.targetNode.height * (1 - this.targetNode.anchorY) * Math.abs(this.targetNode.scaleY)))
        //let cameraPos = LevelManager.controlCom.cameraControl.node.position
        //this.node.setPosition(pos.subtract(cc.v2(cameraPos.x + cc.winSize.width / 2, cameraPos.y + cc.winSize.height / 2)))
    }
}


/**
 * 注意：已把原脚本注释，由于脚本变动过大，转换的时候可能有遗落，需要自行手动转换
 */
// // Learn TypeScript:
// //  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// // Learn Attribute:
// //  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// // Learn life-cycle callbacks:
// //  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
// 
// import Utils, { AudioCtrl, LevelManager } from "../base/GameCore";
// const {ccclass, property} = cc._decorator;
// 
// @ccclass
// export default class Dialogue extends cc.Component {
// 
//     @property({
//         tooltip: "对话内容和点击继续节点",
//         type: [cc.Label]
//     })
//     contents: cc.Label[] = []
// 
//     // LIFE-CYCLE CALLBACKS:
// 
//     // onLoad () {}
//     targetNode: cc.Node = null
// 
//     start () {
// 
//     }
// 
//     setDialog (node: cc.Node, string: string, params = {}) {
//         this.targetNode = node
//         // 节点的缩放情况变化时，使得节点始终为正常缩放
//         //this.update(0.013);
//         // let num = Utils.randInt(3) + 1;
//         // if (string.length >= 40) {
//         //     AudioCtrl.playSe("crazydaveextralong" + num, 1);
//         // } else if (string.length >= 20) {
//         //     AudioCtrl.playSe("crazydavelong" + num, 1);
//         // } else {
//         //     AudioCtrl.playSe("crazydaveshort" + num, 1);
//         // }
//         this.contents[0].string = string
//         this.update(0.013)
//         
//     }
// 
// 
//     close () {
//         this.node.destroy();
//     }
// 
//     update (dt) {
//         let pos = this.targetNode.convertToWorldSpaceAR(cc.v2(0, this.targetNode.height * (1 - this.targetNode.anchorY) * Math.abs(this.targetNode.scaleY)))
//         let cameraPos = LevelManager.controlCom.cameraControl.node.position
//         this.node.setPosition(pos.subtract(cc.v2(cameraPos.x + cc.winSize.width / 2, cameraPos.y + cc.winSize.height / 2)))
//     }
// }
