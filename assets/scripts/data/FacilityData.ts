import AudioCtrl from '../core/AudioCtrl';
import DataManager from '../core/DataManager';
import UserManager from '../core/UserManager';
import Utils from '../core/Utils';
import GalaxyData from './GalaxyData';
import PlanetData from './PlanetData';

/** 
 * !#zh 设施数据对象。 
 * @class FacilityData 
 */
export default class FacilityData {
    type: number = 0
    level: number = 1   //0级表示建筑未建造
    broken: boolean = false
    running: boolean = true

    static energyPerLevel() {
        return UserManager.techChecked(0, 2) ? 5 : 10
    }
    /**
     * 根据类型生成新建筑
     * @param type 类型id
     */
    constructor(type: number, level: number = 1) {
        this.type = type
        this.level = level
    }

    // 注意这个只是生成一个存储的对象当字典用，不要写函数
    // 这是因为存档的数据是读localstorage的json转换来的，对象没有函数
    // 需要写函数，写成静态的，请仿照如下写：

    /**
     * 得到建筑升级的花销
     * @param data 建筑数据，如果为数字，则是直接建造到对应等级的总花销
     * @returns number[3], undefined为无法建造/升级
     * @example cost = FacilityData.getUpgradeCost(facility)
     */
    static getUpgradeCost(data: FacilityData | number, level: number = 1) {
        const dataFacility = DataManager.dataAssets['dataFacility']
        if (data instanceof Object) {
            return dataFacility[data.type]['price']![data.level]
        } else {
            // 建造新建筑直接到x级，是各级花费的和
            let result = [0, 0, 0]
            for (let i = 0; i < level; i++) {
                const cost = dataFacility[data]['price']![i]
                cost.forEach((num: number, j: number) => {
                    result[j] += num
                });
            }
            return result
        }
    }

    /**
     * 得到降级的返回资源数量
     * @param data 设施
     * @param level 
     * @returns 
     */
    static getDegradeReturn(data: FacilityData | number, level: number = 1) {
        const dataFacility = DataManager.dataAssets['dataFacility']
        const rate = UserManager.techChecked(1, 2) ? 0.8 : 0.5
        let type = data instanceof Object ? data.type : data
        level = data instanceof Object ? data.level : level
        // 升到本级花费*50%
        const cost = dataFacility[type]['price']![level - 1]
        return cost.map((num: number, j: number) => {
            return num * rate
        });
    }

    /**
     * 得到拆除的返回资源数量
     * @param data 设施
     * @param level 
     * @returns 
     */
    static getRemoveReturn(data: FacilityData | number, level: number = 1) {
        const dataFacility = DataManager.dataAssets['dataFacility']
        const rate = UserManager.techChecked(1, 2) ? 0.8 : 0.5
        let type = data instanceof Object ? data.type : data
        level = data instanceof Object ? data.level : level
        // 升到本级花费*50%
        let result = [0, 0, 0]
        for (let i = 0; i < level; i++) {
            const cost = dataFacility[type]['price']![i]
            cost.forEach((num: number, j: number) => {
                result[j] += num * rate
            });
        }
        return result
    }

    /**
     * 得到建筑损坏的维修费用，默认为总投资50%
     * @param data 
     * @param level 
     * @returns [res0,res1,res2]
     */
    static getFixCost(data: FacilityData | number, level: number = 1) {
        const dataFacility = DataManager.dataAssets['dataFacility']
        const rate = UserManager.techChecked(2, 2) ? 0.2 : 0.5
        let type = data instanceof Object ? data.type : data
        level = data instanceof Object ? data.level : level
        // 建筑总投资*50%，基地额外修复费用
        let result = type == 0 ? [0, 1000, 250] : [0, 0, 0]
        for (let i = 0; i < level; i++) {
            const cost = dataFacility[type]['price']![i]
            cost.forEach((num: number, j: number) => {
                result[j] += num * rate
            });
        }
        return result
    }

    static getFacilityName(data: FacilityData | number, level: number = 1) {
        let type = data instanceof Object ? data.type : data
        level = data instanceof Object ? data.level : level
        const facilityData = DataManager.dataAssets['dataFacility'][type]
        let oriName = facilityData.name
        if (facilityData['price'].length > 1) {
            // 满级不是一级的建筑，加一下罗马后缀
            oriName += ' ' + 'I'.repeat(level)
        }
        return oriName
    }

    /**
     * 返回建筑运行状态
     * @param galaxy 
     * @param data 
     * @returns 0正常，1低耗，2停止，3损坏
     */
    static getState(galaxy: GalaxyData, data: FacilityData) {
        if (data.broken) {
            return 3
        } else if (!data.running) {
            return 0
        } else {
            return GalaxyData.getState(galaxy) == 2 && data.type >= 2 && data.type <= 4 ? 1 : 0
        }
    }
    
    /**
     * 返回建筑运行状态的文本
     * @param galaxy 
     * @param data 
     * @returns 0正常，1低耗，2停止，3损坏
     */
    static getStateString (galaxy: GalaxyData, data: FacilityData) {
        const str = ['正常', '低耗', '暂停', '损坏']
        return str[FacilityData.getState(galaxy, data)]
    }

    static hasUpgradeTech(data: FacilityData | number, level: number = 1) {
        let type = data instanceof Object ? data.type : data
        level = data instanceof Object ? data.level : level
        switch (type) {
            case 0:     // 基地
                switch (level) {
                    case 1:
                        return UserManager.techChecked(1, 3)
                    case 2:
                        return UserManager.techChecked(1, 4)
                    default:
                        return true
                }
                // return level == 2 || UserManager.techChecked(1, 3)
            case 4:     // 行星防御系统
                return UserManager.techChecked(2, 3)
            case 5:     // 戴森球
                return UserManager.techChecked(0, 3)
            default:
                switch (level) {
                    case 1:
                        return UserManager.techChecked(type - 1, 1)
                    case 2:
                        return UserManager.techChecked(type - 1, 6)
                    default:
                        return true
                }
        }
    }

    /**
     * 得到星球可建的最低建筑等级
     * @param planet 
     * @param type 
     * @returns 
     */
    static getMinLevel (planet: PlanetData, type: number) {
        if (type >= 1 && type <= 3) {
            return planet.hard[type - 1] + 1
        } else {
            return 1
        }
    }

    /**
     * 得到建筑存储量
     * @param data 建筑数据
     * @returns capacity: number[] = []//三个等级，每个等级对应三种矿
     */
    static getCapacity(data: FacilityData | number, level: number = 1): number[] {
        let type = data instanceof Object ? data.type : data
        level = data instanceof Object ? data.level : level
        const facilityData = DataManager.dataAssets['dataFacility'][type]
        if (facilityData.hasOwnProperty('capacity')) {
            return facilityData['capacity'][level - 1]
        }
        return [0, 0, 0]
    }

    static getOptPrice(galaxy: GalaxyData, planet: PlanetData, data: FacilityData | number, upgrade = true): number[] | undefined {
        if (data instanceof Object) {
            if (upgrade || data.broken) {
                return data.broken ? FacilityData.getFixCost(data) : FacilityData.getUpgradeCost(data)
            }
        } else {
            // 建造
            let minLv = FacilityData.getMinLevel(planet, data)
            return FacilityData.getUpgradeCost(data, minLv)
        }
        return undefined
    }

    /**
     * 可否对建筑进行操作
     * @param galaxy 
     * @param planet 建筑所在行星
     * @param data 建筑对象，若为数字则为新建筑type
     * @param upgrade 是否是升级操作
     */
    static canOperate(galaxy: GalaxyData, planet: PlanetData, data: FacilityData | number, upgrade = true) {
        const price = FacilityData.getOptPrice(galaxy, planet, data, upgrade)
        if (price) {
            console.log('br', GalaxyData.getResEnough(galaxy, price));
            
        }
        return !price || GalaxyData.getResEnough(galaxy, price)
    }

    /**
     * 对建筑进行操作
     * @param galaxy 
     * @param planet 建筑所在行星
     * @param data 建筑对象，若为数字则为新建筑type
     * @param upgrade 是否是升级操作
     * @returns 
     */
    static operate(galaxy: GalaxyData, planet: PlanetData, data: FacilityData | number, upgrade = true) {
        const price = FacilityData.getOptPrice(galaxy, planet, data, upgrade)
        // 先排除不能升级的时机，排除完之后先扣钱
        if (price && !GalaxyData.getResEnough(galaxy, price)) {
            AudioCtrl.playSet('invalid')
            return false
        } else if (price) {
            GalaxyData.gainResource(galaxy, price, true)
        }
        
        // 设置效果
        if (data instanceof Object) {
            if (data.broken) {
                // 修复
                AudioCtrl.playSet('equip')
                data.broken = false
                console.log(`${galaxy.name} 上 ${planet.name} 的 ${FacilityData.getFacilityName(data)} 被修复`); 
                if (data.type == 0) galaxy.running = true

            } else if (upgrade) {
                // 升级
                AudioCtrl.playSet('equip')
                data.level++
                console.log(`${galaxy.name} 上 ${planet.name} 的 ${FacilityData.getFacilityName(data)} 升至 ${data.level} 级`); 
            } else {
                // 降级或拆除
                AudioCtrl.playSet('load')
                let minLv = FacilityData.getMinLevel(planet, data.type)
                let payBack = data.level == minLv ? FacilityData.getRemoveReturn(data) : FacilityData.getDegradeReturn(data)
                GalaxyData.gainResource(galaxy, payBack)
                if (data.level == minLv) {
                    console.log(`${galaxy.name} 上 ${planet.name} 的 ${FacilityData.getFacilityName(data)} 被拆除`); 
                    Utils.delete(planet.buildings, data)
                } else {
                    data.level--
                    console.log(`${galaxy.name} 上 ${planet.name} 的 ${FacilityData.getFacilityName(data)} 降至 ${data.level} 级`); 
                }
            }
        } else {
            // 建造
            const minLv = FacilityData.getMinLevel(planet, data)
            const newFacility = new FacilityData(data, minLv)
            planet.buildings.push(newFacility)
            console.log(`${galaxy.name} 上 ${planet.name} 建造了 ${newFacility.level} 级的 ${FacilityData.getFacilityName(newFacility)}`); 
            AudioCtrl.playSet('equip')
        }
        UserManager.saveData()
    }

}
