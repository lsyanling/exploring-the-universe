/** 
 * !#zh 星系数据对象。 
 * @class GalaxyData 
 */
import DataManager from '../core/DataManager';
import Utils from '../core/Utils';
import PlanetData from '../data/PlanetData';
import FacilityData from './FacilityData';
export default class GalaxyData {
    name: string = ''
    planets: PlanetData[] = [] //星球数组
    resourceSurplus: number[] = [0, 0, 0] //能源资源剩余量[能源剩余量，资源1剩余量，资源2剩余量]
    running: boolean = false
    settleTime: number = 0
    haveOverflow: boolean[] = [false, false, false];  //溢出只会提示一次
    firstLoss: boolean = true   //该星系是否到达过能源剩余量不足消耗量的状态
    firstReduce: boolean = true //该星系是否到达过能源净减少状态
    /**
     * 根据类型生成新星系
     * @param seed 类型id,0是太阳系
     */
    constructor(seed: number) {
        if (seed == 0) {
            this.name = '太阳系';
            this.resourceSurplus = [3000, 3000, 3000]   // 初始给的资源
            const planets = DataManager.dataAssets['dataSolar']
            planets.forEach((planet: any) => {
                this.planets.push(new PlanetData(planet));
            });
            // 地球自动建一个基地，变为已探索
            this.planets[3].buildings.push(new FacilityData(0))
            this.planets[3].explore = true;
            this.running = true
        }
        //随机生成算法
        else {
            //随机生成星系名称
            while (true) {
                this.name = 'M' + getRandom(0, 9) + getRandom(0, 9);
                //防止重名
                if (checkgalaxy(this.name)) {
                    break;
                }
            }

            //随机生成星系资源
            this.resourceSurplus = [0, 0, 0];
            // for (let i = 0; i < 3; i++) {
            //     this.resourceSurplus[i] = 1000 + Math.floor(6000 * seededRandom(1, 0));
            // }

            //星球数量随机，5到15个不等
            let planetsNumber = getRandom(5, 15);
            //轨道半径增量
            let minTrackIncrement = getRandom(500, 1000) / planetsNumber;
            //最大公转速度
            let maxOrbitalAngularVelocity = 3 + seededRandom(1, 0) * 5;
            //公转速度减量
            let velocityDecrement = maxOrbitalAngularVelocity / (planetsNumber - 1);
            for (let i = 0; i < planetsNumber; i++) {
                //新的星球对象
                let newPlanet = new PlanetData; //新的星球json
                //随机命名星球
                newPlanet.name = getNewName();
                //如果是恒星
                if (i == 0) {
                    //星球半径
                    newPlanet['radius'] = getRandom(83520, 1183200);
                    //轨道半径
                    newPlanet['orbitalRadius'] = 0;
                    //公转角速度
                    newPlanet['orbitalAngularVelocity'] = 0;
                    //自转角速度
                    newPlanet['spinVelocity'] = 0;
                    //丰度
                    newPlanet['abundance'] = [Math.floor(120 + seededRandom(1, 0) * 240), 0, 0];
                    //难度
                    newPlanet['hard'] = [0, 0, 0];
                }
                //行星
                else {
                    //行星半径最大值：不超过恒星半径
                    let maxRadius = 250222 < this.planets[0]['radius'] ? 250222 : this.planets[0]['radius'];
                    newPlanet['radius'] = getindex(2440, maxRadius);
                    //恒星半径可能大于轨道半径，故单独计算第一颗行星
                    if (i == 1) {
                        //保留四位小数
                        newPlanet['orbitalRadius'] = Math.floor((seededRandom(1, 0) + this.planets[0].radius / 149600000) * 10000) / 10000;
                        newPlanet['orbitalAngularVelocity'] = maxOrbitalAngularVelocity;
                    } else {
                        //轨道增量
                        let trackIncrement = Math.floor((seededRandom(1, 0.1)) * minTrackIncrement);
                        //保留四位小数
                        newPlanet['orbitalRadius'] = Math.floor((this.planets[i - 1].orbitalRadius + trackIncrement) * 10000) / 10000;
                        newPlanet['orbitalAngularVelocity'] = this.planets[i - 1].orbitalAngularVelocity - velocityDecrement * seededRandom(1, 0.5);
                        if (newPlanet['orbitalAngularVelocity'] < 0) {
                            newPlanet['orbitalAngularVelocity'] = this.planets[i - 1].orbitalAngularVelocity / 2;
                        }
                    }
                    newPlanet['spinVelocity'] = 0.05 + Math.pow(seededRandom(1, 0) * 8, 2);
                    for (let j = 0; j < 3; j++) {
                        //丰度
                        newPlanet['abundance'][j] = Math.floor(seededRandom(1, 0) * 50 + 2);
                        let randomNumber = getRandom(0, 15);
                        if (randomNumber < 9) {
                            newPlanet['hard'][j] = 0;
                        } else if (randomNumber < 13) {
                            newPlanet['hard'][j] = 1;
                        } else {
                            newPlanet['hard'][j] = 2;
                        }
                    }

                }
                this.planets.push(newPlanet);
            }

            // 纹理和环
            for (let i = 0; i < this.planets.length; i++) {
                let num = (Math.floor(seededRandom(1, 0) * 100) % 21);
                this.planets[i].texture = 'ground_' + num;
                //大行星随机带环吧
                if (i && num > 12 && this.planets[i].radius > 80000)
                    this.planets[i].ring = 'ring_' + num % 3;
            }
        }

        //生成伪随机数,替代Math.random
        function seededRandom(max: number, min: number) {
            max = max || 1;
            min = min || 0;
            seed = (seed * 9301 + 49297) % 233280;
            let rnd = seed / 233280.0;
            return min + rnd * (max - min);
        }

        //生成m到n之间的（包括m,n）随机整数
        function getRandom(m: number, n: number) {
            return Math.round(seededRandom(1, 0) * (n - m) + m);
        }
        //生成m到n之间的随机整数（指数分布）
        function getindex(m: number, n: number) {
            return Math.round(3 * Math.pow(Math.E, -3 * seededRandom(1, 0) * 4) * (n - m) + m);
        }
        //检查星球名称是否重复，若重复返回false
        function checkplanet(planetName: string) {
            DataManager.planetNames.forEach((value) => {
                if (planetName == value) {
                    return false;
                }
            })
            DataManager.planetNames.push(planetName);
            return true;
        }

        //检查星系名称是否重复
        function checkgalaxy(galaxyName: string) {
            DataManager.galaxyNmaes.forEach((value) => {
                if (galaxyName == value) {
                    return false;
                }
            })
            DataManager.galaxyNmaes.push(galaxyName);
            return true;
        }

        //生成星球随机名称
        function getNewName() {
            let firstNames: string[] = DataManager.dataAssets['galaxyName'];    //得到firstname数组
            let lastNames: string[] = DataManager.dataAssets['galaxylastName'];
            let newName: string = '';
            while (true) {
                //产生两个随机数用来索引名称
                let nameIndex: number[] = [getRandom(0, firstNames.length), getRandom(0, lastNames.length)];
                //如果有这个名称
                if (firstNames[nameIndex[0]] && lastNames[nameIndex[1]]) {
                    newName = firstNames[nameIndex[0]] + lastNames[nameIndex[1]];
                    //判断星球名称是否重复
                    if (checkplanet(newName)) {
                        return newName;
                    }
                }
            }
        }
    }

    /**
     * todo: 得到星系运行状态
     * @param galaxy 星系
     * @returns 0没有基地，1停止运行，2低耗，3正常
     */
    static getState(galaxy: GalaxyData) {
        if (galaxy.running) {
            let capacity = GalaxyData.getProduction(galaxy);
            // 如果净消耗大于能源剩余
            if (capacity[0] - capacity[1] > galaxy.resourceSurplus[0]) { // 低耗运行
                return 2;
            } else { //正常运行
                return 3;
            }
        } else {
            // 如果有基地
            const base = GalaxyData.getFacility(galaxy)
            return base ? 1 : 0
        }
    }

    /**
     * 得到星系运行状态文本
     * @param galaxy 星系
     * @returns 状态文字
     */
    static getStateString(galaxy: GalaxyData) {
        let i = GalaxyData.getState(galaxy)
        // 低耗运行
        // 基地损坏：停止运行
        return ['尚未开拓', '停止运行', '低耗运行', '正常运行'][i]
    }

    static getMaxCapacity(galaxy: GalaxyData) {
        let result = [0, 0, 0];
        const planets = galaxy.planets;
        // 遍历星球
        planets.forEach((planet) => {
            let capacity = PlanetData.getCapacity(planet)
            result = result.map((value, i) => {
                return value + capacity[i]
            })
        })
        return result; // [100,100,100]
    }

    /**
     * 得到星系一个tick(星系中所有星球总和)的能源消耗量和资源产出
     * @param galaxy 星系数据
     * @returns [能源消耗,能源产量,其它资源产量]
     */
    static getProduction(galaxy: GalaxyData) {
        let result = [0, 0, 0, 0];
        //遍历星球
        galaxy.planets.forEach((planet, i) => {
            let production = PlanetData.getProduction(planet)
            //遍历星球上的能源资源数组
            result = result.map((value, i) => {
                return value + production[i]
            })
        })
        return result // [20,50,100,100]
    }

    /**
     * 得到星球净产能：注意低耗状态不能直接加净产能进去
     * @param planet 星球
     * @returns [能源，晶钻，芯片]
     */
    static getProfit(galaxy: GalaxyData) {
        const production = GalaxyData.getProduction(galaxy)
        let result: number[] = [-production[0], 0, 0]
        for (let i = 0; i < result.length; i++) {
            result[i] += production[i + 1]
        }
        return result
    }

    /**
     * 检测资源是否足够
     * @param galaxy 
     * @param resource 资源检测
     * @returns 
     */
    static getResEnough(galaxy: GalaxyData, resource: number[]) {
        return !resource.some((num, i) => {
            return galaxy.resourceSurplus[i] < num;
        })
    }

    static getResFull (galaxy: GalaxyData) {
        let capacity = GalaxyData.getMaxCapacity(galaxy)
        return capacity.map((max, i) => {
            return galaxy.resourceSurplus[i] > max;
        })
    }

    static gainProduction (galaxy: GalaxyData, capacity: number[]) {
        let notes = []
        let fullOld = GalaxyData.getResFull(galaxy)
        let maxCapacity = GalaxyData.getMaxCapacity(galaxy)
        for (let i = 0; i < 3; i++) {
            if (!fullOld[i]) galaxy.resourceSurplus[i] += capacity[i + 1];
        }
        let fullNew = GalaxyData.getResFull(galaxy)
        for (let i = 0; i < 3; i++) {
            if (fullNew[i]) {
                galaxy.resourceSurplus[i] = maxCapacity[i];
                if (!galaxy.haveOverflow[i]) {
                    galaxy.haveOverflow[i] = true;
                    switch (i) {
                        case 0: {
                            notes.push(galaxy.name + '能源溢出。');
                            break;
                        }
                        case 1: {
                            notes.push(galaxy.name + '晶钢溢出。');
                            break;
                        }
                        case 2: {
                            notes.push(galaxy.name + '芯髓溢出。');
                            break;
                        }
                    }
                }
            }
        }
        return notes
    }
    
    static gainResource(galaxy: GalaxyData, resource: number[], cost: boolean = false) {
        resource.forEach((num, i) => {
            galaxy.resourceSurplus[i] += cost ? -num : num
        })
    }

    /**
     * 得到第一个特定类型建筑
     * @param galaxy 星系
     * @param type 建筑类型
     * @returns 
     */
    static getFacility (galaxy: GalaxyData, type = 0) {
        for (let i = 0; i < galaxy.planets.length; i++) {
            for (let j = 0; j < galaxy.planets[i].buildings.length; j++) {
                const facility = galaxy.planets[i].buildings[j];
                if (facility.type === type) {
                    return facility
                }
            }
        }
        return null
    }

    /**
     * 按星系刷新下一个游戏时刻
     * @param galaxy 星系数据
     * @returns 日志数据
     */
    static updateGalaxy(galaxy: GalaxyData) {
        let notes: any[] = [];
        // 得到产能，计算能源供给情况，并给出日志
        notes = GalaxyData.updateRes(galaxy)
        .concat(GalaxyData.updateEmergency(galaxy))
        // 概率突发事件，并给出相应警示
        return notes
    }
    /**
     * 按星球刷新下一个游戏时刻
     * @param galaxy 星球数据
     * @returns notes
     */
    public static updateEmergency(galaxy: GalaxyData) {
        let notes: string[] = []
        const rateBase = 0.002
        //突发事件：只对已探索或有设施的行星有效，且不发生在地球
        galaxy.planets.forEach((planet: PlanetData, i) => {
            if (i != 0) {
                let maxFacNum = PlanetData.maxFacilityNum(planet)
                for (let j = 0; j < maxFacNum; j++) {
                    // 小行星
                    let rand = Math.random()
                    if (rand <= rateBase) {
                        const defendSystem = GalaxyData.getFacility(galaxy, 4);
                        let buildText = ''
                        if (planet.buildings[j]) {
                            const facility = planet.buildings[j]
                            const name = FacilityData.getFacilityName(facility)
                            if (defendSystem && GalaxyData.getState(galaxy) === 3) {
                                buildText = '该小行星按预定路径会击中 ' + name + ' ，目前已被行星防御系统击毁。'
                            } else {
                                // 如果本来就是坏的，直接炸没(已经损坏过，说明没有资源了)
                                if (facility.broken) {
                                    buildText = '命中已经损坏的设施 ' + name + ' ，使得该设施完全损毁。'
                                    Utils.delete(planet.buildings, facility)
                                } else {
                                    buildText = '命中设施 ' + name + ' ，使得该设施损坏。'
                                    facility.broken = true;
                                    facility.level = 1;
                                }
                            }
                        }
                        let res = [0, 0, 0];
                        for (let k = 0; k < res.length; k++) {
                            res[k] = Math.floor(Math.random() * 400 + 80);
                        }
                        GalaxyData.gainResource(galaxy, res);
                        let resText: string = `我们挖掘了该小行星，获取到${res[0]}能源，${res[1]}晶钢，${res[2]}芯髓。`
                        notes.push(`${galaxy.name} 星系的 ${planet.name} 遭到外来小行星撞击，${buildText} ${resText}`);
                        continue;
                    }
                    // 矿难
                    rand = Math.random()
                    if (planet.buildings[j]) {
                        const facility = planet.buildings[j]
                        const name = FacilityData.getFacilityName(facility)
                        const resType = facility.type - 1
                        if (resType >= 0 && resType <= 2 && !facility.broken) {
                            const resName = DataManager.dataAssets['dataSystem']['resName'][resType]
                            let rate = rateBase * 0.8 + planet.minedOres[resType] / PlanetData.exhaustion * rateBase * 3.2
                            if (rand < rate) {
                                facility.broken = true
                                let capacity = FacilityData.getCapacity(facility)
                                capacity[resType] = Math.floor(Math.min(capacity[resType], galaxy.resourceSurplus[resType] * 0.8))
                                GalaxyData.gainResource(galaxy, capacity, true)
                                // 信息
                                notes.push(`${galaxy.name} 星系 ${planet.name}上的 ${name} 由于过度挖掘发生矿难，导致矿业设施损坏，且损失 ${capacity[resType]} ${resName}。`);
                            }
                        }
                    }
                }
            }
        })
        return notes;
    }

    /**
     * 更新星系资源能源
     * @param galaxy 星系
     * @returns notes   日志
     */
    private static updateRes(galaxy: GalaxyData): any[] {
        let notes: any[] = [];
        let production = GalaxyData.getProduction(galaxy)
        // 能源产量不足用量
        if (production[0] > production[1]) {
            // 如果能源剩余量不足消耗量，产能衰减，能源不变
            if (galaxy.resourceSurplus[0] < production[0]) {
                let rate = (galaxy.resourceSurplus[0] + production[1]) / production[0];
                //资源增加，产能衰减
                for (let i = 1; i < 3; i++) {
                    production[i + 1] += production[i + 1] * rate
                }
                notes = notes.concat(GalaxyData.gainProduction(galaxy, production))
                galaxy.resourceSurplus[0] = 0
                // 如果这个时刻是第一次到这个状态，给出日志提示
                if (galaxy.firstLoss) {
                    notes.push(galaxy.name + '的现有能源不足以维持生产，进入低耗运行状态。');
                    galaxy.firstLoss = false;
                }
            } else {    // 能源有剩余，会减少，
                //能源净减少
                galaxy.resourceSurplus[0] -= production[0];
                //能源，资源增加
                notes = notes.concat(GalaxyData.gainProduction(galaxy, production))
                //如果这个时刻是第一次到这个状态，给出日志提示
                if (galaxy.firstReduce) {
                    notes.push(galaxy.name + '的能源产量不足用量，正消耗剩余能源用以生产');
                    galaxy.firstReduce = false;
                }
            }
        } else {
            //能源净增加
            galaxy.resourceSurplus[0] -= production[0];
            //资源增加
            notes = notes.concat(GalaxyData.gainProduction(galaxy, production))
        }
        // 更新枯竭度
        galaxy.planets.forEach(planet => {
            PlanetData.updateExhaustion(planet);
        });
        return notes;
    }

}
