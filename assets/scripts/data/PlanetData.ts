import FacilityData from '../data/FacilityData';
import GalaxyData from './GalaxyData';
import UserManager from '../core/UserManager';

/** 
 * !#zh 行星数据对象。 
 * @class PlanetData 
 */
export default class PlanetData {
    type: number = 0
    name: string = ''
    radius: number = 0  //星球半径
    texture: string = ''
    ring: string = ''
    orbitalRadius: number = 0   //轨道半径：轨道半径为0是恒星
    orbitalAngularVelocity: number = 0  //轨道角速度
    spinVelocity: number = 0    //自旋速度
    abundance: number[] = [0, 0, 0]   //丰度 能源，资源1，资源2
    hard: number[] = [0, 0, 0]    //开采难度
    explore: boolean = false //是否探索
    buildings: FacilityData[] = []  //星球建筑
    minedOres: number[] = [0, 0, 0]  // 最新调整： 资源挖掘量，根据这个算枯竭程度（高等级建筑挖的更快了），具体看引用这个变量的代码
    // 显示在界面中的半径，但是实际还需要加上0.8*i，这里便于其他地方调用
    radiusInUI = Math.sqrt(this.orbitalRadius) * 3;

    static exhaustion = 1200 // 600次资源耗竭
    /**
     * 根据类型生成新星球
     * @param data 不带有完整数据的星球对象
     */
    constructor(data: any = {}) {
        for (const key in this) {
            if (data.hasOwnProperty(key)) {
                this[key] = data[key]
            }
        }
    }

    /**
     * todo: 得到星球可建造的设施列表
     * @param planet 星球
     * @param galaxy 星球所在星系
     * @returns 可建设施的类型列表
     */
    static availableFacilities(planet: PlanetData, galaxy: GalaxyData): number[] {
        const fixed = planet.orbitalRadius === 0
        const base = GalaxyData.getFacility(galaxy)
        if (fixed) {
            return (base && UserManager.techChecked(0, 3)) ? [5] : []
        } else {
            if (base) {
                let available: number[] = [];   
                const hasBulit = planet.buildings.map(value => {
                    return value.type
                })
                // 三种矿井
                const buildings: number[] = [1, 2, 3];
                available = buildings.filter(type => {
                    if (hasBulit.indexOf(type) >= 0) return false
                    const minLv = FacilityData.getMinLevel(planet, type)
                    return FacilityData.hasUpgradeTech(type, minLv - 1)
                })
                // 行星防御系统
                const defendSystem = GalaxyData.getFacility(galaxy, 4)
                if (!defendSystem && FacilityData.hasUpgradeTech(4)) available.push(4)
                return available;
            } else {
                return [0]
            }
        }
    }

    /**
     * 星球可建最多设施数量
     * @param planet 星球
     * @returns 
     */
    static maxFacilityNum(planet: PlanetData) {
        if (planet.orbitalRadius === 0) {
            return 1
        } else if (planet.radius >= 25000) {
            return 3
        } else if (planet.radius >= 5000) {
            return 2
        } else {
            return 1
        }
    }

    /**
     * 得到星球资源容量
     * @param planet 星球
     */
    static getCapacity(planet: PlanetData) {
        let result: number[] = [0, 0, 0]
        planet.buildings.forEach(building => {
            //遍历仓储量
            let capacity = FacilityData.getCapacity(building)
            result = result.map((value, i) => {
                return value + capacity[i]
            })
        });
        return result
    }

    /**
     * 得到资源剩余
     * @param planet 星球
     * @param i 资源id
     */
    static getResidue(planet: PlanetData, i: number) {
        let rate = Math.max(PlanetData.exhaustion - planet.minedOres[i], 0) / PlanetData.exhaustion
        return Math.pow(rate, 3) * 0.8 + 0.2
    }

    /**
     * 得到星球能源消耗以及各资源产能
     * @param planet 星球
     * @returns 
     */
    static getProduction(planet: PlanetData) {
        let result: number[] = [0, 0, 0, 0]
        planet.buildings.forEach(building => {
            if (!building.running || building.broken) return
            // 计算能源消耗(建筑等级)
            if (building.type >= 2 && building.type <= 4) {
                result[0] += building.level * FacilityData.energyPerLevel()
            }
            if (building.type >= 1 && building.type <= 3) {
                const i = building.type - 1
                // 资源生产公式
                result[i + 1] += (building.level + 1) * planet.abundance[i] * PlanetData.getResidue(planet, i) * UserManager.getProductionBuff(i)
            } else if (building.type == 5) {
                // 戴森球产率公式
                result[1] += (building.level + 1) * planet.abundance[0] * UserManager.getProductionBuff(0)
            }
        });
        return result
    }

    static getResFactor(planet: PlanetData, i: number) {

        return planet.abundance[i] * PlanetData.getResidue(planet, i)
    }

    /**
     * 得到星球净产能
     * @param planet 星球
     * @returns 
     */
    static getProfit(planet: PlanetData) {
        const production = PlanetData.getProduction(planet)
        let result: number[] = [-production[0], 0, 0]
        for (let i = 0; i < result.length; i++) {
            result[i] = result[i] + production[i + 1]
        }
        return result
    }

    /**
     * 刷新资源开采程度
     * @param planet 星球
     */
    static updateExhaustion(planet: PlanetData) {
        planet.buildings.forEach(building => {
            if (!building.running || building.broken) return
            // 计算能源消耗(建筑等级)，戴森球能源不会枯竭
            if (building.type >= 1 && building.type <= 3) {
                const i = building.type - 1
                planet.minedOres[i] += UserManager.techChecked(0, 4) ? (building.level + 1) * 0.5 : building.level + 1
                if (planet.minedOres[i] > PlanetData.exhaustion) planet.minedOres[i] = PlanetData.exhaustion
            }
        });
    }
}
