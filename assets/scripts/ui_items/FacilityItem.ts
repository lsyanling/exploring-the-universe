
import { _decorator, Component, Node, Button, color, Prefab, SpriteFrame, Sprite } from 'cc';
import DataManager from '../core/DataManager';
import UserManager from '../core/UserManager';
import FacilityData from '../data/FacilityData';
import GalaxyData from '../data/GalaxyData';
import PlanetData from '../data/PlanetData';
import UiItem from '../ui_base/UiItem';
import DivBlock from '../ui/DivBlock';
const { ccclass, property } = _decorator;

@ccclass('FacilityItem')
export class FacilityItem extends UiItem {
    @property({
        tooltip: '升级降级两个按钮。',
        type: [Sprite]
    })
    buttons: Sprite[] = []

    @property({
        tooltip: '按钮的图片[升级/降级/修复/移除]。',
        type: [SpriteFrame]
    })
    icons: SpriteFrame[] = []

    @property({
        tooltip: '小模块的layout',
        type: Node
    })
    layout: Node = null!

    @property({
        type: Prefab,
        tooltip: '需要的预制'
    })
    itemTiny: Prefab = null!

    // [1]
    // dummy = '';

    // [2]
    // @property
    // serializableDummy = 0;

    setValue(galaxy: GalaxyData, planet: PlanetData | null = null, data: FacilityData | null = null) {
        this.node.children[this.node.children.length - 1].active = !data
        for (let i = 0; i < this.node.children.length - 1; i++) {
            this.node.children[i].active = !!data
        }
        if (data) {
            const dataFacility = DataManager.dataAssets['dataFacility'][data.type]
            // 1. 渲染文字
            let list: any = {
                name: FacilityData.getFacilityName(data),
                state: FacilityData.getStateString(galaxy, data),
                layout: []
            }
            let stateColor = color('#ffffff')
            // todo: 判断state：正常，暂停，损坏，低耗，及颜色

            // 能源消耗
            if (data.type >= 2 && data.type <= 4) {
                // 矿井：计算产出
                let item = ['res_sub_0', data.level * FacilityData.energyPerLevel()]
                list.layout.push(item)
            }
            // 设施产出
            if (data.type >= 1 && data.type <= 3 || data.type == 5) {
                const i = data.type == 5 ? 0 : data.type - 1
                let production = PlanetData.getResFactor(planet!, i) * (data.level + 1) * UserManager.getProductionBuff(i)
                let item = [`res_add_${i}`, production.toFixed(1)]
                list.layout.push(item)
            }
            // 计算各资源容量
            for (let i = 0; i < 3; i++) {
                let capacity = dataFacility['capacity'][data.level - 1][i]
                if (capacity != 0) {
                    let item = [`res_sto_${i}`, capacity]
                    list.layout.push(item)
                }
            }
            DivBlock.render(this.node, list, true)
            
            if (data.broken) {
                this.buttons[0].node.active = false
                this.buttons[1].node.active = true
                this.buttons[1].spriteFrame = this.icons[2]
            } else {
                this.buttons[0].node.active = FacilityData.getUpgradeCost(data) && FacilityData.hasUpgradeTech(data) ? true : false
                this.buttons[1].node.active = data.type != 0
                this.buttons[1].spriteFrame = data.level == FacilityData.getMinLevel(planet!, data.type) ? this.icons[3] : this.icons[1]
            }

        }
    }

    // update (deltaTime: number) {
    //     // [4]
    // }
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.0/manual/en/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.0/manual/en/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.0/manual/en/scripting/life-cycle-callbacks.html
 */
