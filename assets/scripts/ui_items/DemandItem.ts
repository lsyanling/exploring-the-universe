
import { _decorator, Component, Node, RichText, Label, Sprite } from 'cc';
import UiItem from '../ui_base/UiItem';
import DivBlock from '../ui/DivBlock';
const { ccclass, property } = _decorator;

@ccclass('DemandItem')
export class DemandItem extends UiItem {
    key: RichText = null!
    value: Label = null!
    icon: Sprite = null!
    // [1]
    // dummy = '';

    // [2]
    // @property
    // serializableDummy = 0;

    setValue (key: string, value: string, ok: boolean | null = false) {
        let list: any = {
            key: key,
            value: value,
        }
        const icon = this.node.getChildByName('.icon')!
        if (ok != null) { 
            list.icon = 'judge_' + ok
        }
        icon.active = ok != null
        DivBlock.render(this.node, list)
        
    }
    // update (deltaTime: number) {
    //     // [4]
    // }
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.0/manual/en/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.0/manual/en/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.0/manual/en/scripting/life-cycle-callbacks.html
 */
