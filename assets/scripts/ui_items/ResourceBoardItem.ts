
import { _decorator, Component, Node, color, Label, ProgressBar } from 'cc';
import GalaxyData from '../data/GalaxyData'
import UiItem from '../ui_base/UiItem';
import DivBlock from '../ui/DivBlock';
const { ccclass, property } = _decorator;

@ccclass('ResourceBoardItem')
export class ResourceBoardItem extends UiItem {
    // [1]
    // dummy = '';

    // [2]
    // @property
    // serializableDummy = 0;

    
    setValue (data: GalaxyData, i = 0) {
        let profit = GalaxyData.getProfit(data).map(value => {
            let abs = Math.abs(value)
            if (value > 0) {
                return '+' + abs.toFixed(1)
            } else if (value < 0) {
                return '-' + abs.toFixed(1)
            }
            return ''
        })
        let capacity = GalaxyData.getMaxCapacity(data)
        let list: any = {
            icon: 'icon_res_' + i,
            value: data.resourceSurplus[i].toFixed(0),
            addition: profit[i],
            progress: capacity[i] == 0 ? 0 : data.resourceSurplus[i] / capacity[i]
        }
        list = DivBlock.render(this.node, list)

        if (list.addition) {
            let label = list.addition as Label
            if (list.addition.string[0] === '+') {
                label.color = color('#99ff99')
            } else if (list.addition.string[0] === '-') {
                label.color = color('#ff9999')
            } else {
                label.color = color('#ffff99')
            }
        }
        if (list.progress) {
            let progress = list.progress as ProgressBar
            progress.node.active = capacity[i] != 0
        }
    }


    // update (deltaTime: number) {
    //     // [4]
    // }
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.0/manual/en/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.0/manual/en/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.0/manual/en/scripting/life-cycle-callbacks.html
 */
