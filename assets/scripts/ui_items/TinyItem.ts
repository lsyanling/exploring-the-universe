
import { _decorator, Component, Node, Label, Widget, UITransform } from 'cc';
import UiItem from '../ui_base/UiItem';
import DivBlock from '../ui/DivBlock';
const { ccclass, property } = _decorator;

@ccclass('TinyItem')
export class TinyItem extends UiItem {
    setValue (icon: string, value: string) {
        // const widget = this.getComponent(Widget)
        // if (widget) widget.updateAlignment()
        let list: any = {
            icon: icon,
            value: value
        }
        DivBlock.render(this.node, list)
        for (const key in list) {
            const com = list[key] as Component
            const widget = com.getComponent(Widget)
            if (widget) widget.updateAlignment()
            
        }
    }
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.0/manual/en/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.0/manual/en/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.0/manual/en/scripting/life-cycle-callbacks.html
 */
