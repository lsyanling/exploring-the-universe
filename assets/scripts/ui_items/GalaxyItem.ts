
import { _decorator, Component, Node, resources } from 'cc';
import GalaxyData from '../data/GalaxyData';
import UiItem from '../ui_base/UiItem';
import DivBlock from '../ui/DivBlock';
const { ccclass, property } = _decorator;

@ccclass('GalaxyItem')
export class GalaxyItem extends UiItem {
    // [1]
    // dummy = '';

    // [2]
    // @property
    // serializableDummy = 0;

    setValue (data: GalaxyData) {
        let list: any = {
            name: data.name,
            state: GalaxyData.getStateString(data),
            resource: []
        }
        let profit = GalaxyData.getProfit(data)
        let capacity = GalaxyData.getMaxCapacity(data)
        for (let i = 0; i < profit.length; i++) {
            list.resource.push([`icon_res_${i}`, data.resourceSurplus[i], capacity[i], profit[i]])
        }
        DivBlock.render(this.node, list, true)
    }

    // update (deltaTime: number) {
    //     // [4]
    // }
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.0/manual/en/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.0/manual/en/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.0/manual/en/scripting/life-cycle-callbacks.html
 */
