
import { _decorator, Component, Node, Label, Sprite } from 'cc';
import UiItem from '../ui_base/UiItem';
import DivBlock from '../ui/DivBlock';
const { ccclass, property } = _decorator;

export interface ValueChangeItemList {
    key: string,
    value: string,
    value_old?: string
    unit_old?: string
    unit?: string
}

@ccclass('ValueChangeItem')
export class ValueChangeItem extends UiItem {
    arrow: Node = null!
    old: Label = null!
    oldUnit: Label = null!
    // [1]
    // dummy = '';

    setValue (key: string, newValue: string, unit: string = '', oldValue: string = '') {
        if (!this.arrow) {
            this.arrow = this.node.children[0]!
            this.old = this.node.getChildByName('.value_old')!.getComponent(Label)!
        }
        this.arrow.active = !!oldValue
        let list: ValueChangeItemList = {
            key: key,
            value: unit ? newValue + ' ' + unit : newValue,
        }
        if (oldValue) {
            list.value_old = unit ? oldValue + ' ' + unit : oldValue
            this.old.node.active = true
        } else {
            this.old.node.active = false
        }
        DivBlock.render(this.node, list)
    }
    // [2]
    // @property
    // serializableDummy = 0;

    start () {
        // [3]
    }

    // update (deltaTime: number) {
    //     // [4]
    // }
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.0/manual/en/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.0/manual/en/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.0/manual/en/scripting/life-cycle-callbacks.html
 */
