
import { _decorator, Component, Node, color } from 'cc';
import UiItem from '../ui_base/UiItem';
import DivBlock from '../ui/DivBlock';
const { ccclass, property } = _decorator;

@ccclass('ResourceItem')
export class ResourceItem extends UiItem {
    // [1]
    // dummy = '';

    // [2]
    // @property
    // serializableDummy = 0;

    setValue (icon: string, value: number, max: number, addition: number = 0) {
        let abs = Math.abs(addition)
        let addText = ''
        let c = '#99ff99'
        if (addition > 0) {
            addText = '+'+abs.toFixed(1)
        } else if (addition < 0) {
            addText = '-'+abs.toFixed(1)
            c = '#ff9999'
        }
        let list: any = {
            icon: icon,
            now: value.toFixed(0),
            max: max.toFixed(0),
            addition: addText
        }
        list = DivBlock.render(this.node, list)
        list.addition.color = color(c)
    }

    // update (deltaTime: number) {
    //     // [4]
    // }
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.0/manual/en/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.0/manual/en/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.0/manual/en/scripting/life-cycle-callbacks.html
 */
