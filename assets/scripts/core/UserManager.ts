import { sys } from "cc"
import GalaxyData from "../data/GalaxyData"
import DataManager from "./DataManager"

export interface TechData {
    name: string,
    description: string,
    cost: number
}

const sign = 'userData'
/** 
 * !#zh 玩家信息管理器，负责游戏存读档以及现有物品装备的存储。 
 * @class UserManager
 * @static
 */ 

export default class UserManager {

    public static userData: any = null
    /**
     * 清空用户存档
     */
    public static clearSaveData() {
        UserManager.userData = {
            settings: {
                mute: false,
                musicVolume: 0.8,
                soundVolume: 0.8,
                skip: false
            },
            version:"Ver 1.0.0",
            // 后面加别的内容，如存档数据
            galaxies: {},   // 星系seed=>情况
            nowGalaxy: 0,   // 现在所在星系的seed
            techChecked: [[],[],[]],
            time: 0,        // 游戏时间
            pause: false    // 手动设置暂停
        }
        UserManager.saveData()
        // console.log('已重置存档');
        
    }

    /** 
     * !#zh 保存数据。
     * @method saveData
     * @return {Boolean} 
     */ 

    public static saveData(): boolean {
        let data = JSON.stringify(UserManager.userData)
        sys.localStorage.setItem(sign, data);
        return true;
    }

    /** 
     * !#zh 读取数据。
     * @method loadData
     * @return {Boolean} 
     */ 

    public static loadData(): boolean {
        let data: string = ''
        let userData = null
        let dataChanged = true;
        try {
            data = sys.localStorage.getItem(sign);
            userData = JSON.parse(data)
        } catch (error: any) {
            UserManager.clearSaveData()
            throw new Error(error);
        }
        if (!userData || !userData.version) {
            UserManager.clearSaveData()
            return true;
        } else {
            // 版本继承
            // switch (userData.version) {
            //     case "Ver 0.0.1":
            //     case "Ver 0.0.2":
            //         userData.version = "Ver 0.0.3"
            //         break;
            //     default:
            //         dataChanged = false;
            //         break;
            // }
            UserManager.userData = userData
            if (dataChanged) UserManager.saveData()
            return true;
        };
    }

    /** 
     * !#zh 读取特定用户设定。 
     * @method getUserSettings
     * @param name 要读取的设定的名称
     * @return 
     */ 

    public static getUserSettings(name: string = '') {
        if (name) {
            if (UserManager.userData.settings.hasOwnProperty(name)) {
                return UserManager.userData.settings[name]
            }
            return false;
        } else {
            return UserManager.userData.settings
        }
    }

    // 接下来具体写读取/检验详细数据的方法，供组件使用
    
    /** 
     * !#zh 读取特定星系。 
     * @method getGalaxy
     * @param seed 要读取的星系的种子，-2为读取所有星系，-1为读取当前星系
     * @returns 
     */ 

    public static getGalaxy(seed: number = -2) {
        if (UserManager.userData.hasOwnProperty('galaxies')) {
            if (seed == -2) {
                return UserManager.userData['galaxies']
            } else {
                if (seed == -1) seed = UserManager.userData['nowGalaxy']
                let galaxies = UserManager.userData['galaxies']
                if (galaxies.hasOwnProperty(seed)) {
                    return galaxies[seed]
                }
                return false
            }
        } else {
            // 存档没有星系：错误，重置存档
            UserManager.clearSaveData()
            // console.log('存档没有星系：错误，重置存档');
            return seed == -2 ? {} : false
        }
    }

    /**
     * 刷新游戏时刻
     * @returns 日志信息
     */
    public static updateTick() {
        UserManager.userData.time++
        const galaxies = UserManager.userData['galaxies']
        let notes: any = {}
        // 刷新各个星系运行数据,遍历星系，刷新星系群中所有星系游戏时刻
        for (const seed in galaxies) {
            if (Object.prototype.hasOwnProperty.call(galaxies, seed)) {
                const galaxy: GalaxyData = galaxies[seed];
                notes[seed] = GalaxyData.updateGalaxy(galaxy);
            }
        }
        // 存档
        UserManager.saveData()
        return notes
    }
    /**
     * getExploreCost
     */
    public static getExploreCost() {
        let num = Object.keys(UserManager.userData['galaxies']).length
        let result = UserManager.techChecked(2, 4) ? [2000 + 2000 * num, 500 + 500 * num, 1000 * num] : [5000 + 2500 * num, 2000 + 500 * num, 1500 * num]
        return result
    }

    public static getSeedNumber (x: string, y: string, z: string) {
        let nx = Number(x)
        let ny = Number(y)
        let nz = Number(z)
        return nx * 1000000 + ny * 1000 + nz
    }

    public static checkSeedOk(seed: number) {
        return !UserManager.userData['galaxies'].hasOwnProperty(seed)
    }

    public static registerNewGalaxy (seed: number) {
        UserManager.userData['galaxies'][seed] = new GalaxyData(seed)
        UserManager.userData['nowGalaxy'] = seed
        UserManager.saveData()
    }

    /**
     * static canSetNewTech
     */
    public static canSetTech(branch: number, i: number) {
        if (i == 0) return true
        let demand = i < 5 ? i - 1 : i % 5
        return UserManager.userData['techChecked'][branch].indexOf(demand) != -1
    }

    /**
     * 是否研发了科技
     * @param i 分支
     * @param j 分支的科技序号
     */
    public static techChecked(i: number, j: number) {
        return UserManager.userData['techChecked'][i].indexOf(j) != -1
    }

    /**
     * 得到科技树对资源生产的加成
     * @param i 资源序号
     * @returns 
     */
    public static getProductionBuff(i: number) {
        let list = [[1.2, 1.6],[1.2, 1.6],[1.2, 1.6]]
        // let list = [[80, 80],[80, 80],[80, 80]]
        return UserManager.techChecked(i, 5) ? list[i][1] : UserManager.techChecked(i, 0) ? list[i][0] : 1
    }

    public static getTimeString (time: number = -1) {
        if (time < 0) time = UserManager.userData['time'] 
        let year = 2021 + Math.floor((time + 8) / 12)  // 是9月开始,
        let month = (time + 8) % 12 + 1                // 1-12月分别是0-11
        return year + '年' + month + '月'
    }
    /**
     * static getPosString
     */
    public static getPosString(seed: number) {
        let x = Math.floor(seed / 1000000)
        let y = Math.floor(seed % 1000000 / 1000)
        let z = Math.floor(seed % 1000)
        return `X:${x}  Y:${y}  Z:${z}`
    }

    static setGalaxyName(galaxy: GalaxyData, name:string) {
        if (!name) return false
        for (const key in UserManager.userData['galaxies']) {
            if (Object.prototype.hasOwnProperty.call(UserManager.userData['galaxies'], key)) {
                const galaxy = UserManager.userData['galaxies'][key];
                console.log('check', galaxy.name, name, galaxy.name == name);
                if (galaxy.name == name) {
                    return galaxy.name
                }
            }
        }
        galaxy.name = name
        UserManager.saveData()
        return galaxy.name
    }

    static getTransferCost (res: number[] = []) {
        const rate = UserManager.techChecked(0, 7) ? 0.05 : 0.1
        let num = 0
        res.forEach(element => {
            num += Math.ceil(rate * element)
        });
        return num
    }

    /**
     * 资源转运
     * @param from 从星系
     * @param to 转到目标星系
     * @param list 资源转运清单
     */
    static resTransfer(from: GalaxyData, to: GalaxyData, list: number[]) {
        let cost = UserManager.getTransferCost(list)
        GalaxyData.gainResource(to, list)
        list[0] += cost
        GalaxyData.gainResource(from, list, true)
        // from和to两星系向下取整
        from.resourceSurplus = from.resourceSurplus.map(value => {
            return Math.max(0, Math.floor(value))
        })
        to.resourceSurplus = to.resourceSurplus.map(value => {
            return Math.max(0, Math.floor(value))
        })
        console.log(`${from.name}向${to.name}转移了${list}物资，其中消耗${cost}`);
        
        UserManager.saveData()
    }

    /**
     * 研发某项科技
     * @param galaxy 要花费
     * @param b 分支
     * @param i 项目
     * @returns 
     */
    static setTech(galaxy: GalaxyData, b: number, i: number) {
        const data: TechData = DataManager.dataAssets['dataTechTree'][b][i]
        if (galaxy.resourceSurplus[2] >= data.cost && UserManager.canSetTech(b, i)) {
            galaxy.resourceSurplus[2] -= data.cost
            UserManager.userData['techChecked'][b].push(i)
            console.log(`研发${b}科技第${i}项`);
            UserManager.saveData()
            return true
        }
        return false
    }
}
