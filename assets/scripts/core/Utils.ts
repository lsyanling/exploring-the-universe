export default class Utils {
    /**
     * 生成一个范围在(0, max-1)内的整数.
     *
     * @static
     * @method randInt
     * @param max 数上限（不包括）
     * @return 一个随机整数
     */
    public static randInt = function(max: number) {
        return Math.floor(max * Math.random());
    }

    /**
     * 删除数组第一个出现的元素
     * @param arr 数组
     * @param val 元素
     */
    public static delete(arr: any[], val: any) {
        var index = arr.indexOf(val); 
        if (index > -1) { 
        arr.splice(index, 1); 
        }
    }
    /**
     * 按条件删除数组中所有元素
     * @static
     * @param array 要操作的数组
     * @param condition 条件函数
     * @param target 调用函数的对象
     */
    public static cleanArray(array: any[], condition: Function, target: Object = Utils) {
        // for (let i = 0; i < array.length; i++) {
        // if (target && condition.call(target, array[i], i, array)) {
        // cc.js.array.fastRemove(array, array[i])
        // i--
        // } else if (condition(array[i], i, array)) {
        // cc.js.array.fastRemove(array, array[i])
        // i--
        // }
        // }
    }
    /**
     * 从数组随机取出元素
     * @static
     * @param arr 数组
     * @param count 取出元素数目
     */
    
    public static getRandomArrayElements(arr: any[], count: number) {
        let shuffled = arr.slice(0), i = arr.length, min = i - count, temp, index;
        while (i-- > min) {
            index = Math.floor((i + 1) * Math.random());
            temp = shuffled[index];
            shuffled[index] = shuffled[i];
            shuffled[i] = temp;
        }
        return shuffled.slice(min);
    }

}
