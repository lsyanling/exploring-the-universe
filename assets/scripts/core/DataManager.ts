/** 
 * !#zh 数据管理器，负责游戏设定数据的加载。 
 * @class DataManager
 * @static
 */ 
export default class DataManager {

    public static dataAssets: any = {}
    public static planetNames: string[] = [] //记录已有的星球名称，防止重复
    public static galaxyNmaes: string[] = [] //记录已有的星系名称，防止重复
    /** 
     * !#zh 检查数据文件是否读取
     * @method dataInited
     */ 
    public static dataInited = function (data: string) {
        return DataManager.dataAssets.hasOwnProperty(data);
    }

}
