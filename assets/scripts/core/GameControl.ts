// Learn TypeScript:
//  - https://docs.cocos.instance/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.instance/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.instance/creator/manual/en/scripting/life-cycle-callbacks.html

import { _decorator, Component, JsonAsset, Prefab, assetManager, find, game, log, Node, Label, director, AudioSource, assert, SpriteAtlas } from 'cc';
import AudioCtrl from './AudioCtrl';
const {ccclass, property} = _decorator;

import DataManager from './DataManager';
import UserManager from './UserManager';

@ccclass('GameControl')
export default class GameControl extends Component {
    public static instance: GameControl = null!
    @property({
        tooltip: '游戏需要预读取的所有数据文件',
        type: [JsonAsset]
    })
    dataAssets: JsonAsset[] = []
    @property({
        tooltip: '需要预读取的所有图集文件',
        type: [SpriteAtlas]
    })
    atlasAssets: SpriteAtlas[] = []
    @property({
        tooltip: '关卡需要预读取的所有预制文件',
        type: [Prefab]
    })
    prefabAssets: Prefab[] = []
    scenes = []
    config: any = null
    loadingNode: Node = null!
    
    // LIFE-CYCLE CALLBACKS:
    onLoad () {
        const canvas = find('Canvas')!
        if (canvas) {
            canvas.active = false
            // 设为常驻节点
            game.addPersistRootNode(this.node);
            GameControl.instance = this
            this.loadingNode = this.node.getChildByName('Loading')!
    
            // 初始化所有 dataAsset
            this.dataAssets.forEach(data => {
                DataManager.dataAssets[data.name] = data.json
            });
            // 读取存档
            if (UserManager.loadData()) {
                console.log("成功读取存档");
                console.log(UserManager.userData);
            }
            let source = this.getComponents(AudioSource)!
            // 加载并设置分包 
            assetManager.loadBundle('audio', (err, bundle) => {
                if (err) {
                    log('Error url [' + err + ']');
                    return;
                }
                // console.log('读取成功');
                
                AudioCtrl.setBundle(source[0], source[1], bundle)
                // 读取并设置完音乐分包后才正式开始初始化标题
                canvas.active = true
            });
            
        }
    }

    start () {

    }

    /**
     * 读取画面
     * @param name 画面名字
     * @param config 要写入的设置
     * @returns 
     */
    loadScene (name: string, config: any = null) {
        this.loadingNode.active = true
        director.loadScene(name, (err) => {
            if (err) {
                let label = this.node.getComponentInChildren(Label)!
                label.string = err.toString()
            } else {
                this.loadingNode.active = false
            }
        })
        this.config = config
        return true;
    }
}