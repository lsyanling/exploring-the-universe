import { AssetManager, AudioClip, AudioSource } from "cc"
import DataManager from "./DataManager"
import UserManager from "./UserManager"

/** 
 * !#zh 音乐音效播放管理器，所有音乐音效播放工作都在这里进行，受设置音量统一控制。 
 * @class AudioCtrl 
 * @static
 */ 

export default class AudioCtrl {
    public static bgmVolume: number = 0
    public static bgsVolume: number = 0
    private static bundle: AssetManager.Bundle = null!
    private static musicSource: AudioSource = null!
    private static soundSource: AudioSource = null!
    
    public static setBundle (musicSource: AudioSource, soundSource: AudioSource, bundle: AssetManager.Bundle) {
        AudioCtrl.musicSource = musicSource
        AudioCtrl.soundSource = soundSource
        AudioCtrl.bundle = bundle
        // 读取预置se
        for (const key in DataManager.dataAssets['dataSystem']['se']) {
            const element = DataManager.dataAssets['dataSystem']['se'][key];
            if (element) {
                AudioCtrl.bundle.load('se/'+element, AudioClip, (err, cilp: AudioClip) => {
                    if (err) {
                        console.log('音效预读取出错',err,element,cilp);
                        
                    }
                })
            }
        }
    }

    /** 
     * !#zh 播放背景音乐，文件放在audio/bgm下。 
     * @method PlayBgm 
     * @param {String | AudioClip} name 音乐的名称或者Audioclip资源
     * @param {Number} volume 音乐的音量
     * @example 
     * playBgm('backgroundmusic', 1); 
     */ 

    public static playBgm = function(name: string | AudioClip, volume: number = 1) {
        if (!AudioCtrl.bundle) return 
        let realVolume = volume * AudioCtrl.musicVolume()
        
        if (typeof name == 'string') {
            if (name && AudioCtrl.musicSource.clip && AudioCtrl.musicSource.clip.name === name) {
                // 正在播放当前音乐的情况
                AudioCtrl.adjustMusicVolume()
            } else {
                // 无音乐播放，或当前播放的不是当前音乐
                AudioCtrl.musicSource.stop();
                AudioCtrl.bundle.load('bgm/'+name, AudioClip, function (err, clip) {
                    AudioCtrl.musicSource.clip = clip
                    AudioCtrl.musicSource.volume = realVolume
                    AudioCtrl.musicSource.play()
                });
            }
        } else {
            if (AudioCtrl.musicSource.clip == name) {
                // 正在播放当前音乐的情况
                AudioCtrl.adjustMusicVolume()
            } else {
                // 无音乐播放，或当前播放的不是当前音乐
                AudioCtrl.musicSource.stop();
                AudioCtrl.musicSource.clip = name
                AudioCtrl.musicSource.volume = realVolume
                AudioCtrl.musicSource.play()
            }
        }
        AudioCtrl.bgmVolume = volume
    }

    /** 
     * !#zh 播放音效，文件放在audio/se下。 
     * @method PlaySe 
     * @param {String | AudioClip} name 音乐的名称或者Audioclip资源
     * @param {Number} volume 音乐的音量
     * @example 
     * playSe('soundeffect', 1); 
     */ 

    public static playSe = function(name: string | AudioClip, volume: number = 1) {
        if (!AudioCtrl.bundle) return 
        let realVolume = volume * AudioCtrl.soundVolume()
        if (typeof name == 'string') {
            AudioCtrl.bundle.load('se/'+name, AudioClip, function (err: any, clip: AudioClip) {
                AudioCtrl.soundSource.playOneShot(clip, realVolume)
            }.bind(AudioCtrl));
        } else {
            AudioCtrl.soundSource.playOneShot(name, realVolume)
        }
    }
    /** 
     * !#zh 播放设置中的se。 
     * @method PlaySet
     */ 

    public static playSet = function(se: string) {
        let seName = DataManager.dataAssets['dataSystem']["se"][se]
        if (seName) {
            AudioCtrl.playSe(seName, 1);
        } else {
            throw new Error("找不到 " + se + 'SE');
            
        }
    }
    /** 
     * !#zh 在设置音乐音量的时候，调整背景音乐的音量。 
     * @method adjustMusicVolume 
     */ 
    public static adjustMusicVolume = function() {
        if (AudioCtrl.musicSource.clip) {
            AudioCtrl.musicSource.volume = AudioCtrl.bgmVolume * AudioCtrl.musicVolume()
        }
    }

    /** 
     * !#zh 在设置音效音量的时候，调整背景音效的音量。 
     * @method adjustSoundVolume 
     */ 

    public static adjustSoundVolume = function() {
        // if (AudioCtrl.playingBGS[0]) {
        //     audioEngine.setVolume(AudioCtrl.playingBGS[1], AudioCtrl.playingBGS[2] * AudioCtrl.soundVolume())
        // }
    }

    /** 
     * !#zh 得到设置的音乐音量。 
     * @method musicVolume 
     */ 

    public static musicVolume = function() {
        return UserManager.getUserSettings('mute') ? 0 : UserManager.getUserSettings('musicVolume')
    }
 
    /** 
     * !#zh 得到设置的音效音量。 
     * @method soundVolume 
     */ 

    public static soundVolume = function() {
        return UserManager.getUserSettings('mute') ? 0 : UserManager.getUserSettings('soundVolume')
    }

}
