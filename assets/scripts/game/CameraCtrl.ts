
import { _decorator, Component, Node, systemEvent, SystemEventType, EventKeyboard, EventMouse, Vec3, RigidBody, Vec4, Quat, find, Vec2, macro, ToggleContainer, Toggle } from 'cc';
import UserManager from '../core/UserManager';
import GalaxyManager from './GalaxyManager';
import SceneGalaxy from '../ui/SceneGalaxy';
import DivBlock from '../ui/DivBlock';
const { ccclass, property } = _decorator;

@ccclass('CamaraCtrl')
export class CamaraCtrl extends Component {
    // private isMouseDown = false; //当前鼠标是否按下
    private touchesAbs: number = 0;
    onLoad() {
        let lv = new Vec3(0, 0, 0);
        let rb = this.node.getComponent(RigidBody);
        // systemEvent.on(Node.EventType.MOUSE_DOWN, (e: EventMouse) => {
        //     console.log('鼠标按下233');
        //     this.isMouseDown = true;
        // }, this);

        // // 鼠标松开
        // systemEvent.on(Node.EventType.MOUSE_DOWN, (e: EventMouse) => {
        //     // console.log('鼠标松开1');
        //     this.isMouseDown = false;
        // }, this);

        // 检测手指数量
        systemEvent.on(SystemEventType.TOUCH_START, (touch, ev) => {
            let touches = ev.getAllTouches();
            let touchNum = touches.length;
            console.log(touchNum);
            if (touchNum === 2) {
                let touchPoint1 = touches[0].getLocation();
                let touchPoint2 = touches[1].getLocation();
                this.touchesAbs = Vec2.distance(touchPoint1, touchPoint2);
            }

        }, this);

        // 鼠标移动
        systemEvent.on(SystemEventType.TOUCH_MOVE, (touch, ev) => {
            // if (!this.isMouseDown) return;
            // console.log('鼠标移动1');
            let touches = ev.getAllTouches();
            let touchNum = touches.length;
            // console.log(touchNum);

            if (touchNum === 1) {
                let delta = touch.getDelta();
                let av = new Vec3(0, 0, 0);
                let rb = this.node.getComponent(RigidBody);
                let angle_speed = 0.02;

                rb?.getAngularVelocity(av);

                // 左右视角移动
                if (delta.x < 0) {
                    av.y = -angle_speed * Math.pow(-delta.x, 1.5);
                } else if (delta.x > 0) {
                    av.y = angle_speed * Math.pow(delta.x, 1.5);
                }

                // 上下视角移动
                if (delta.y > 0) { //向上
                    av.x = angle_speed * Math.pow(delta.y, 1.5);
                } else if (delta.y < 0) { // 向下
                    av.x = -angle_speed * Math.pow(-delta.y, 1.5);
                }

                av.z = 0;

                rb?.setAngularVelocity(av);

                let angle = this.node.eulerAngles;
                let angleX = angle.x;

                // if (angleX > 80)
                //     angleX = 80;
                // if (angleX < -80)
                //     angleX = -80;

                this.node.eulerAngles = new Vec3(angleX, angle.y, 0);
            }
            else if (touchNum === 2) {
                let touchPoint1 = touches[0].getLocation();
                let touchPoint2 = touches[1].getLocation();
                // this.touchesAbs = Vec2.distance(touchPoint1, touchPoint2);
                let newTouchesAbs: number;
                newTouchesAbs = Vec2.distance(touchPoint1, touchPoint2);
                // let forward_speed = 4;
                // 移动端捏合移动摄像机速度 随当前星系大小改变
                let seed = UserManager.userData['nowGalaxy']
                let galaxyNode: Node | null = find('Galaxy');
                let galaxyObject: GalaxyManager | null | undefined = galaxyNode?.getComponent(GalaxyManager);
                let i = galaxyObject?.galaxies[seed].planets.length - 1;
                let forward_speed = galaxyObject?.galaxies[seed].planets[i].orbitalRadius / 10;

                rb?.getLinearVelocity(lv);

                if (newTouchesAbs > this.touchesAbs) {
                    lv.z = forward_speed;
                }
                else if (newTouchesAbs < this.touchesAbs) {
                    lv.z = -forward_speed;
                }
                lv.x = 0;
                lv.y = 0;
                Vec3.transformQuat(lv, lv, this.node.getRotation());

                rb?.setLinearVelocity(lv);
                this.touchesAbs = newTouchesAbs;
            }

        }, this);

        // 键盘移动
        systemEvent.on(SystemEventType.KEY_DOWN, (event: EventKeyboard) => {

            // let delta = touch.getDelta();
            let delta = new Vec2(10, 10);

            let av = new Vec3(0, 0, 0);
            let rb = this.node.getComponent(RigidBody);
            let angle_speed = 0.02;

            rb?.getAngularVelocity(av);

            //上下移动
            switch (event.keyCode) {
                //W
                case macro.KEY.w: {
                    av.x = -angle_speed * Math.pow(delta.y, 1.5);
                    break;
                }
                //S
                case macro.KEY.s: {
                    av.x = angle_speed * Math.pow(delta.y, 1.5);
                    break;
                } default: break;
            }

            //左右移动
            switch (event.keyCode) {
                //A
                case macro.KEY.a: {
                    av.y = angle_speed * Math.pow(delta.x, 1.5);
                    break;
                }
                //D
                case macro.KEY.d: {
                    av.y = -angle_speed * Math.pow(delta.x, 1.5);
                    break;
                }
                default: break;
            }

            //45°移动
            switch (event.keyCode) {
                //Q
                case macro.KEY.q: {
                    av.x = -angle_speed * Math.pow(delta.y, 1.5);
                    av.y = angle_speed * Math.pow(delta.x, 1.5);
                    break;
                }
                //E
                case macro.KEY.e: {
                    av.x = -angle_speed * Math.pow(delta.y, 1.5);
                    av.y = -angle_speed * Math.pow(delta.x, 1.5);
                    break;
                }
                //Z
                case macro.KEY.z: {
                    av.x = angle_speed * Math.pow(delta.y, 1.5);
                    av.y = angle_speed * Math.pow(delta.x, 1.5);
                    break;
                }
                //X
                case macro.KEY.x: {
                    av.x = angle_speed * Math.pow(delta.y, 1.5);
                    av.y = -angle_speed * Math.pow(delta.x, 1.5);
                    break;
                }
                default: break;
            }

            av.z = 0;

            rb?.setAngularVelocity(av);

            let angle = this.node.eulerAngles;
            let angleX = angle.x;

            // if (angleX > 80)
            //     angleX = 80;

            // if (angleX < -80)
            //     angleX = -80;

            this.node.eulerAngles = new Vec3(angleX, angle.y, 0);

        }, this);

        // todo: 空格聚焦恒星、取消星球选中：稍微往左偏的时候有特性，建议保留该特性
        systemEvent.on(SystemEventType.KEY_DOWN, (event: EventKeyboard) => {
            if (event.keyCode === macro.KEY.space) {

                let test: Node = find("Canvas/Scene_Planet/ListView")!;
                let togglecontainer = test.getComponent(ToggleContainer)!;
                for (let i = 0; i < togglecontainer.toggleItems.length; i++) { //遍历每一个Toggle
                    if (togglecontainer.toggleItems[i]?.isChecked) { //若选中
                        console.log(i);
                        let ty: Toggle = togglecontainer.toggleItems[i]!;
                        ty.isChecked = false; //设为false
                    }
                }
                //聚焦的星系
                let focusGalaxyComponet = find('Galaxy')?.getComponent(GalaxyManager);
                focusGalaxyComponet?.focus();

                //focusGalaxyComponet?.setRunning(true);//继续使星系旋转

            }
        }, this);
    }

    update() {
        let lv = new Vec3(0, 0, 0);
        let rb = this.node.getComponent(RigidBody);

        systemEvent.on(SystemEventType.MOUSE_WHEEL, (e: EventMouse) => {
            // console.log('鼠标滚动');
            let scoll = e.getScrollY();

            // 滚轮移动时向前的速度
            // let forward_speed = 4;
            // 滚轮移动摄像机速度 随当前星系大小改变
            let seed = UserManager.userData['nowGalaxy']
            let galaxyNode: Node | null = find('Galaxy');
            let galaxyObject: GalaxyManager | null | undefined = galaxyNode?.getComponent(GalaxyManager);
            let i = galaxyObject?.galaxies[seed].planets.length - 1;
            let forward_speed = galaxyObject?.galaxies[seed].planets[i].orbitalRadius / 10;

            rb?.getLinearVelocity(lv);

            if (scoll > 0) {
                // console.log('向上滚动了');
                lv.z = -forward_speed;
            } else if (scoll < 0) {
                // console.log('向下滚动了');
                lv.z = forward_speed;
            }
            scoll = 0;
            lv.x = 0;
            lv.y = 0;
            Vec3.transformQuat(lv, lv, this.node.getRotation());

            rb?.setLinearVelocity(lv);
            //console.log("#############");
            ////////////////////
            // let rotation = new Quat()
            // // console.log(this.node.getWorldPosition(rotation));
            // // console.log(rotation);
            // this.node.getWorldRotation(rotation);
            // console.log(rotation);


            // console.log("#############");
            ///////////////
        }, this);

    }

}

