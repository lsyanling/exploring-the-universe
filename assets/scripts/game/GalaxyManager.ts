
import { _decorator, Component, Node, absMax, Prefab, resources, Texture2D, ImageAsset, tween, Vec3, Camera, Vec4, Quat } from 'cc';
import UserManager from '../core/UserManager';
import PlanetData from '../data/PlanetData';
import SceneGalaxy from '../ui/SceneGalaxy';
import DivBlock from '../ui/DivBlock';
import Planet from '../object/Planet';
const { ccclass, property } = _decorator;

@ccclass('GalaxyManager')
export default class GalaxyManager extends Component {
    // 可在编辑器可见的属性

    @property({
        type: Prefab,
        tooltip: '需要的预制'
    })
    planet: Prefab = null!

    @property({
        type: Camera,
        tooltip: '摄像机'
    })
    camera: Camera = null!

    // 不可在编辑器可见的属性
    static instance: GalaxyManager = null!
    galaxies: any = null!
    timer: number = 0
    sceneCtrl: SceneGalaxy = null!
    running: boolean = false
    waiting: boolean = false
    okNumber: number = 0
    loadingOk: boolean = false
    static textureCache: any = {}
    static nowShownGalaxy: string = null!

    // 这里开始写各种生命周期方法和自定义方法
    onLoad() {
        GalaxyManager.instance = this
    }

    preLoad(seed: number) {
        let date = new Date()
        console.log('读取开始' + date.toLocaleTimeString());
        const galaxy = this.galaxies[seed]
        let allNum = galaxy.planets.length * 2
        this.okNumber = 0
        this.loadingOk = false
        let loadList: string[] = []
        galaxy.planets.forEach((data: PlanetData, i: number) => {
            loadList.push(data.texture)
            if (data.ring) {
                loadList.push(data.ring)
            }
        });
        resources.load(loadList, (err: any, images: ImageAsset[]) => {
            // console.log(err, texture);
            if (images.length === loadList.length) {
                images.forEach((image, i) => {
                    if (!GalaxyManager.textureCache.hasOwnProperty(loadList[i])) {
                        let texture = new Texture2D()
                        texture.image = image
                        GalaxyManager.textureCache[loadList[i]] = texture
                    }
                })// 不能直接限定类型Texture2d读就很离谱
                this.loadingOk = true
                let date = new Date()
                console.log('读取完成' + date.toLocaleTimeString());
                // console.log(images);
                if (this.waiting) {
                    this.sceneCtrl.callPlanet()
                    this.setGalaxy()
                }

            }

        })
    }

    checkOkTexture(err: any, texture: any) {
        GalaxyManager.textureCache

    }

    setGalaxy() {
        this.waiting = false
        this.loadingOk = false
        let seed = UserManager.userData['nowGalaxy']
        const galaxy = this.galaxies[seed]
        if (GalaxyManager.nowShownGalaxy != seed) {
            DivBlock.childrenStackSort(this.node, galaxy.planets.length, this.planet)
            galaxy.planets.forEach((data: PlanetData, i: number) => {
                const planet = this.node.children[i].getComponent(Planet)!
                planet.setPlanetData(data, i)
            });
            GalaxyManager.nowShownGalaxy = seed
        }
        this.requestRunning(true)
        tween(this.node)
        .to(0.5, { scale: new Vec3(1, 1, 1) })
        .start()

    }

    hideGalaxy(callback: Function = null!, arg: any = null!) {
        if (callback) {
            tween(this.node)
                .to(0.5, { scale: new Vec3() })
                .call(callback.bind(arg))
                .start()
        } else {
            tween(this.node)
                .to(0.5, { scale: new Vec3() })
                .start()

        }

    }

    update(dt: number) {
        if (this.running) {
            this.timer += dt;
            //每五帧刷新一次
            if (this.timer >= 5) {
                this.updateTick();
                this.timer -= 5;
            }
        }
    }

    /**
     * 刷新下一个游戏时刻
     */
    updateTick() {
        let notes = UserManager.updateTick()
        // 渲染日志
        if (UserManager.userData.time == 1) {
            let description = '人类在地球上经历了漫长的文明发展过程，是时候准备冲出太阳系，建立更加广阔的文明了。\n\
凭借地球的基地作为支撑，我们可以在各大星球建立矿区基地，收集太阳系的资源，探索其它星系，建立更广阔的文明。'
            this.sceneCtrl.setEmergency('星际开拓者', description)
        }
        for (var val in notes) {
            let noteString = '';
            if (notes[val].length != 0) {
                for (let note of notes[val]) {
                    noteString = noteString + '\n' + note;
                }
            }
            if (noteString != '') {
                this.sceneCtrl.setEmergency('警告', noteString);
            }
        }
        // 渲染
        this.sceneCtrl.refresh()
    }

    /**
     * 设置游戏是否运行，在进行一些界面操作需要停止
     * @param running 是否运行
     */
    private setRunning(running = true) {
        if (running && !this.running) {
            // 继续行星旋转动作
        } else if (this.running && !running) {
            // 暂停行星旋转动作
        }
        this.running = running
    }

    requestRunning(running = true) {
        console.log('申请正义之心', running);
        
        if (UserManager.userData['pause']) {
            running = false
        }
        this.setRunning(running)
    }

    /**
     * 镜头移到星球上
     * @param i 星球id，-1为取消聚焦
     * 
     */
    focus(i: number = -1) {
        let rotation = new Vec3()
        let cameraRotation = new Quat(0, 1, 0, 6.123234262925839e-17);
        let standScal = new Vec3(2.842609239610561, 2.8426092396105616, 2.842609239610561);//太阳的世界Scal为标准
        let otherScal = new Vec3(1.7800867795983952, 1.7800867795983952, 1.7800867795983952)//以太阳系中的土星为标准
        let iScal = new Vec3();
        //console.log("================");
        console.log(i);
        console.log("改变前相机的位置");
        //{x: 3.12090215858133e-16, y: 0, z: -12.594765663146973}
        if (i == -1) {
            this.node.getChildByName('.' + 0)?.getChildByName("Sphere")!.getWorldPosition(rotation);//获得中心星球的位置
            //////////////////
            this.node.getChildByName('.' + 0)?.getChildByName("Sphere")!.getWorldScale(iScal);//获得中心星球的Scal

            ///////////////
            //若镜头方向不正，则调整镜头方向
            if (this.camera.node.worldRotation != cameraRotation) {
                tween(this.camera.node)
                    .to(0.5, { worldRotation: cameraRotation }, { easing: 'smooth' })
                    .start()
            }

            if (iScal > standScal) { //若此星球的Scal大于太阳的Scal
                console.log("llllllllllllllllllllllllll");

                tween(this.camera.node)//调整位置
                    .to(0.5, { worldPosition: new Vec3(rotation.x, rotation.y, rotation.z - 17) }, { easing: 'smooth' })
                    //.union()
                    .start();
            } else {
                tween(this.camera.node)//再调整位置
                    .to(0.5, { worldPosition: new Vec3(rotation.x, rotation.y, rotation.z - 10) }, { easing: 'smooth' })
                    //.union()
                    .start();
            }
            this.requestRunning(true)
            return;
        }
        console.log("星球的位置");
        this.node.getChildByName('.' + i)?.getChildByName("Sphere")!.getWorldPosition(rotation);//获得星球所在位置
        this.node.getChildByName('.' + i)?.getChildByName("Sphere")!.getWorldScale(iScal);//获得星球的Scal
        console.log(rotation);
        if (iScal >= otherScal) { //若此星球的Scal大于标准
            ///////////////////////////
            // console.log("大星球的尺寸");            
            // let test : Vec3 = new Vec3();
            // this.node.getChildByName('.'+i)?.getChildByName("Sphere")?.getWorldScale(test);
            // console.log(test);
            //////////////////////

            if (this.camera.node.worldRotation != cameraRotation) {//先调整镜头方向
                tween(this.camera.node)
                    .to(0.5, { worldRotation: cameraRotation }, { easing: 'smooth' })
                    .start();
            }

            tween(this.camera.node)//调整镜头位置
                .to(0.5, { worldPosition: new Vec3(rotation.x - 0.2, rotation.y, rotation.z - 7) }, { easing: 'smooth' })
                .start();
            //this.camera.node.setWorldPosition(rotation.x,rotation.y,rotation.z-5);
        } else {
            if (this.camera.node.worldRotation != cameraRotation) {//调整镜头方向
                tween(this.camera.node)
                    .to(0.5, { worldRotation: cameraRotation }, { easing: 'smooth' })
                    .start();
            }

            tween(this.camera.node)//调整镜头位置
                .to(0.5, { worldPosition: new Vec3(rotation.x - 0.2, rotation.y, rotation.z - 2.5) }, { easing: 'smooth' })
                .start();
            //this.camera.node.setWorldPosition(rotation.x,rotation.y,rotation.z-2.3);
        }//改变相机的位置

        console.log("目前相机的位置");
        // let record = new Vec3;
        // // this.camera.node.worldRotation.getEulerAngles(record)
        // this.camera.node.getWorldPosition(record);
        // console.log(record);
        // console.log(rotation);
        //console.log("================");

    }

}

