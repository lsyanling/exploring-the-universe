
import { _decorator, Component, Node, Vec3, SpotLight, SphereLight, Material, MeshRenderer } from 'cc';
import PlanetData from '../data/PlanetData';
import GalaxyManager from '../game/GalaxyManager';
const { ccclass, property } = _decorator;

@ccclass('Planet')
export default class Planet extends Component {

    @property({
        type: Node,
        tooltip: '球体模型'
    })
    sphere: Node = null!;

    @property({
        type: SphereLight,
        tooltip: '球光照'
    })
    light: SphereLight = null!;
    
    @property({
        type: SpotLight,
        tooltip: '光照模型'
    })
    spot: SpotLight = null!;

    @property({
        type: Node,
        tooltip: '是否有环'
    })
    ring: Node = null!;


    // [1]
    // dummy = '';

    // [2]
    // @property
    // serializableDummy = 0;

    data: PlanetData = null!

    start() {
        // [3]
    }

    setPlanetData(data: PlanetData, i: number) {
        this.data = data
        const fixed = data.orbitalRadius == 0
        const scale = Math.log10(data.radius) - 3
        this.sphere.setScale(new Vec3(scale, scale, scale))
        let pos = new Vec3(this.sphere.position)
        pos.x = Math.sqrt(data.orbitalRadius) * 3 + 0.8 * i // 显示在界面中的半径，最好用一个以data.orbitalRadius为自变量的函数变换一下
        this.sphere.setPosition(pos)
        this.light.node.active = fixed
        this.light.size = scale / 2
        this.light.range = scale * 1.3 / 2
        // 行星聚光灯
        this.spot.node.active = !fixed
        this.spot.size = 0.2 * scale
        this.spot.range = 1 * scale
        let newPos = new Vec3(-scale, 0, 0)
        this.spot.node.setPosition(newPos.add(pos))
        // 设置球体材质
        let sphereMesh: MeshRenderer = this.sphere.getComponent(MeshRenderer)!;
        let sphereMaterial: Material = sphereMesh.material!;
        let texture = GalaxyManager.textureCache[data.texture]
        sphereMaterial.setProperty("albedoMap", texture);
        // 设置环材质
        this.ring.active = !!data.ring
        if (this.ring.active) {
            let ringMesh: MeshRenderer = this.ring.getComponent(MeshRenderer)!;
            let ringMaterial: Material = ringMesh.material!;
            let ringTexture = GalaxyManager.textureCache[data.ring]
            ringMaterial.setProperty("albedoMap", ringTexture);
        }
        // 自转公转随机设定
        let rotation = new Vec3()
        this.node.rotation.getEulerAngles(rotation)
        rotation.y += Math.random() * 180 * Math.sqrt(Math.sqrt(this.data.orbitalAngularVelocity))
        this.node.setRotationFromEuler(rotation)
        this.sphere.rotation.getEulerAngles(rotation)
        rotation.y += Math.random() * 180 * Math.sqrt(this.data.spinVelocity)
        this.sphere.setRotationFromEuler(rotation)
    }

    update(dt: number) {
        let running = GalaxyManager.instance.running
        if (running) {
            let rotation = new Vec3()
            this.node.rotation.getEulerAngles(rotation)
            rotation.y += dt * Math.sqrt(Math.sqrt(this.data.orbitalAngularVelocity)) * 3
            this.node.setRotationFromEuler(rotation)
            this.sphere.rotation.getEulerAngles(rotation)
            rotation.y += dt * Math.sqrt(this.data.spinVelocity) * 3
            this.sphere.setRotationFromEuler(rotation)
        }
    }
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.0/manual/en/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.0/manual/en/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.0/manual/en/scripting/life-cycle-callbacks.html
 */
