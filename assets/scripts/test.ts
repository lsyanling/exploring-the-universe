
import { _decorator, Component, Node, Prefab, instantiate, Layers, Vec3, resources, Texture2D, MeshRenderer, Material, systemEvent, SystemEventType } from 'cc';
import { Test2 } from './test2';
const { ccclass, property } = _decorator;
/**
 * 测试：加在galaxy上测试
 */
@ccclass('Test')
export class Test extends Component {

    @property({
        type:Prefab,
        tooltip:'测试'
    })
    test:Prefab = null!;
    // [1]
    // dummy = '';

    // [2]
    // @property
    // serializableDummy = 0;

    

    start () {
        // [3]
    }
    onLoad(){
        this.myTest();
    }

    myTest(){

        const newNode = instantiate(this.test);
        newNode.addComponent(Test2);
        console.log(newNode);
        
        this.node.addChild(newNode)
//         var otherNode=new Node("otherNode");
//         otherNode.parent=this.node;
// //克隆
//         var clone=instantiate(this.test);
//         otherNode.addChild(clone);
//         //this.node.layer=Layers.Enum.UI_2D
        this.node.layer = Layers.Enum.UI_2D;
        console.log("测试测试测试测试测试测试测试测试测试");
        
    }

    

    // update (deltaTime: number) {
    //     // [4]
    // }
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.0/manual/en/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.0/manual/en/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.0/manual/en/scripting/life-cycle-callbacks.html
 */
